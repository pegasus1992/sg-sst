<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Constants;
use app\models\Utils;
use app\models\search\CategoriasSearch;

/* @var $this yii\web\View */
/* @var $usuario app\models\logic\Usuario */
/* @var $categoriasSearch app\models\search\CategoriasSearch */
/* @var $categoriasDataProvider yii\data\ActiveDataProvider */
/* @var $usuariosSearch app\models\search\UsuariosSearch */
/* @var $usuariosDataProvider yii\data\ActiveDataProvider */

$this->title = 'Documentos';
if ($usuario->id != Yii::$app->user->id) {
    $this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['documentos/', 'id' => Yii::$app->user->id]];
    $this->params['breadcrumbs'][] = $usuario->name;
} else {
    $this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="documento-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
//    $this->registerJsFile(
//            '@web/js/loadTime.js', ['depends' => [\yii\web\JqueryAsset::className()]]
//    );
    $tiempo = Utils::mostrarTiempoRestante();
    if (isset($tiempo)) {
        Yii::$app->session->setFlash($tiempo['type'], $tiempo['message']);
    }
    ?>
    <!--div id="tiempo"></div-->

    <?=
    GridView::widget([
        'dataProvider' => $categoriasDataProvider,
        'columns' => [
            [
                'header' => $usuario->id,
                'headerOptions' => ['hidden' => true,],
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
            /* @var $column yii\grid\DataColumn */
            if ($model->tipo == Constants::tipoAuxiliar) {
                $idCategoria = $model->idCategoria;
                $idUsuario = $column->header;
                $params = Yii::$app->request->queryParams;
                $categoriasSearch = new CategoriasSearch();
                $dataProvider = $categoriasSearch->searchSubmenu($idCategoria, $params);
                if (sizeof($dataProvider->models) > 0) {
                    $url = Html::a($model->nombre, ['documentos/menu', 'id' => $idUsuario, 'c' => $idCategoria]);
                } else {
                    $url = Html::a($model->nombre, ['documentos/view', 'id' => $idUsuario, 'c' => $idCategoria, 'fp' => null]);
                }
            } else {
                $url = $model->nombre;
            }

            switch ($model->nivel) {
                case 1:
                    $url = "<b>$url</b>";
                    break;
                case 2:
                    if ($model->tipo == Constants::tipoMayor) {
                        $url = "<ul><i>$url</i></ul>";
                    } else {
                        $url = "<ul>$url</ul>";
                    }
                    break;
                default:
                    $url = "<ul><ul>$url</ul></ul>";
                    break;
            }
            return $url;
        },
            ],
        ],
    ]);
    ?>

    <?php
    if (!is_null($usuariosDataProvider)) {
        ?>
        <h1><?= Html::encode("Proyectos") ?></h1>

        <?=
        GridView::widget([
            'dataProvider' => $usuariosDataProvider,
            'columns' => [
                [
                    'headerOptions' => ['hidden' => true,],
                    'format' => 'raw',
                    'value' => function ($model) {
                $url = Html::a($model->name, ['documentos/', 'id' => $model->id]);
                return $url;
            },
                ], [
                    'headerOptions' => ['hidden' => true,],
                    'attribute' => 'Rol',
                    'format' => 'raw',
                    'value' => function ($model) {
                return Utils::getRol($model->authAssignments[0]->item_name);
            },
                ],
            ],
        ]);
    }
    ?>
</div>
