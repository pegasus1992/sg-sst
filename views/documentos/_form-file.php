<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use app\models\Utils;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Documento */
/* @var $usuario app\models\logic\Usuario */
/* @var $categoria app\models\logic\Categoria */
/* @var $documentoPadre app\models\logic\Documento */

$this->params['breadcrumbs'][] = ['label' => 'Documentos', 'url' => ['documentos/', 'id' => Yii::$app->user->id]];
$categoriaPadre = $categoria->categoriaPadre;
if ($usuario->id != Yii::$app->user->id) {
    $idL = Yii::$app->user->id;
    $usuarioL = Usuario::findOne($idL);
    $rolL = $usuarioL->authAssignments[0]->item_name;
    $rolI = $usuario->authAssignments[0]->item_name;
    $this->params['breadcrumbs'][] = ['label' => $usuario->name, 'url' => ['documentos/', 'id' => $usuario->id]];
}
$rutas = Utils::obtenerRutaCategorias($categoria);
for ($i = sizeof($rutas) - 1; $i > 0; $i--) {
    $this->params['breadcrumbs'][] = $rutas[$i]->nombre;
}
$this->params['breadcrumbs'][] = ['label' => $categoria->nombre, 'url' => ['documentos/view/', 'id' => $usuario->id, 'c' => $categoria->idCategoria, 'fp' => null]]; //
{
    $carpetas = Utils::obtenerRutaFicheros($documentoPadre);
    for ($i = sizeof($carpetas) - 1; $i >= 0; $i--) {
        $docum = $carpetas[$i];
        $carpeta = Utils::decrypt($docum->rutaArchivo);
        $this->params['breadcrumbs'][] = ['label' => $carpeta, 'url' => ['documentos/view', 'id' => $usuario->id, 'c' => $categoria->idCategoria, 'fp' => $docum->idDocumento]];
    }
}
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="documento-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $tiempo = Utils::mostrarTiempoRestante();
    if (isset($tiempo)) {
        Yii::$app->session->setFlash($tiempo['type'], $tiempo['message']);
    }
    ?>

    <div class="documento-form">

        <?php
        $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data']]);
        ?>

        <?=
        $form->field($model, 'archivo')->widget(FileInput::classname(), [
            'pluginOptions' => [
                'allowedFileExtensions' => ['pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'],
                'showUpload' => false,
            ],
        ]);
        ?>

        <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

<?php ActiveForm::end(); ?>

    </div>

</div>
