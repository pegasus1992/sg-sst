<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\logic\Usuario;
use app\models\Utils;
use app\models\Constants;

/* @var $this yii\web\View */
/* @var $usuario app\models\logic\Usuario */
/* @var $categoria app\models\logic\Categoria */
/* @var $documentoPadre app\models\logic\Documento */
/* @var $documentosSearch app\models\search\DocumentosSearch */
/* @var $datosCarpetas yii\data\ActiveDataProvider */
/* @var $datosPlantillas yii\data\ActiveDataProvider */
/* @var $datosArchivos yii\data\ActiveDataProvider */

$this->title = 'Documentos';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['documentos/', 'id' => Yii::$app->user->id]];
if ($usuario->id != Yii::$app->user->id) {
    $idL = Yii::$app->user->id;
    $usuarioL = Usuario::findOne($idL);
    $rolL = $usuarioL->authAssignments[0]->item_name;
    $rolI = $usuario->authAssignments[0]->item_name;
    $this->params['breadcrumbs'][] = ['label' => $usuario->name, 'url' => ['documentos/', 'id' => $usuario->id]];
}
$rutas = Utils::obtenerRutaCategorias($categoria);
for ($i = sizeof($rutas) - 1; $i > 0; $i--) {
    $this->params['breadcrumbs'][] = $rutas[$i]->nombre;
}
$this->params['breadcrumbs'][] = ['label' => $categoria->nombre, 'url' => ['documentos/view', 'id' => $usuario->id, 'c' => $categoria->idCategoria, 'fp' => null]]; //
{
    $carpetas = Utils::obtenerRutaFicheros($documentoPadre);
    for ($i = sizeof($carpetas) - 1; $i >= 0; $i--) {
        $docum = $carpetas[$i];
        $carpeta = Utils::decrypt($docum->rutaArchivo);
        if ($i > 0) {
            $this->params['breadcrumbs'][] = ['label' => $carpeta, 'url' => ['documentos/view', 'id' => $usuario->id, 'c' => $categoria->idCategoria, 'fp' => $docum->idDocumento]];
        } else {
            $this->params['breadcrumbs'][] = $carpeta;
        }
    }
}
?>
<div class="documento-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $tiempo = Utils::mostrarTiempoRestante();
    if (isset($tiempo)) {
        Yii::$app->session->setFlash($tiempo['type'], $tiempo['message']);
    }
    ?>

    <p>
        <?php
        $idFicheroPadre = null;
        if (isset($documentoPadre)) {
            $idFicheroPadre = $documentoPadre->idDocumento;
        }

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
        $roles2 = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR, Constants::NUM_PROYECTO];
        if ($usuario->id == Yii::$app->user->id && Utils::validarPermisosCategoria($roles, $categoria, Constants::ACCION_CREAR_CARPETA) == 1) {
            echo Html::a('Crear Carpeta', ['create-folder', 'c' => $categoria->idCategoria, 'fp' => $idFicheroPadre], ['class' => 'btn btn-success']);
        }
        if ($usuario->id == Yii::$app->user->id && Utils::validarPermisosCategoria($roles, $categoria, Constants::ACCION_SUBIR_PLANTILLA) == 1) {
            echo Html::a('Subir Plantilla', ['create-template', 'c' => $categoria->idCategoria, 'fp' => $idFicheroPadre], ['class' => 'btn btn-primary']);
        }
        if ($usuario->id == Yii::$app->user->id && Utils::validarPermisosCategoria($roles2, $categoria, Constants::ACCION_SUBIR_ARCHIVO) == 1) {
            echo Html::a('Subir Archivo', ['create-file', 'c' => $categoria->idCategoria, 'fp' => $idFicheroPadre], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $datosCarpetas,
        'columns' => [
            [
                'attribute' => $usuario->id,
                'header' => 'Carpeta',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /* @var $model app\models\logic\Documento */
                    /* @var $column yii\grid\DataColumn */
                    $rutaArchivo = $model->rutaArchivo;
                    $rutaArchivo = Utils::decrypt($rutaArchivo);

                    $idUsuario = $column->attribute;
                    return Html::a("<b>$rutaArchivo</b>", ['documentos/view', 'id' => $idUsuario, 'c' => $model->categoria->idCategoria, 'fp' => $model->idDocumento]);
                },
                    ], [
                        'attribute' => 'Usuario',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model app\models\logic\Documento */
                            $idUsuario = $model->idUsuario;
                            $usuarioActualizo = Usuario::findOne($idUsuario);
                            return $usuarioActualizo->name;
                        },
                    ], [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                        'buttons' => [
                            'delete' => function($url, $model) {
                                /* @var $model app\models\logic\Documento */
                                $idUsuarioL = Yii::$app->user->id;
                                $idUsuario = $model->idUsuario;

                                $categoria = $model->categoria;
                                $documentoPadre = $model->ficheroPadre;
                                $idFicheroPadre = null;
                                if (isset($documentoPadre)) {
                                    $idFicheroPadre = $documentoPadre->idDocumento;
                                }

                                $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
                                if ($idUsuario == $idUsuarioL && Utils::validarPermisosCategoria($roles, $categoria, Constants::ACCION_BORRAR_CARPETA) == 1) {
                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                                                'delete-folder',
                                                'doc' => $model->idDocumento,
                                                'c' => $categoria->idCategoria,
                                                'fp' => $idFicheroPadre
                                                    ], [
                                                'class' => '',
                                                'data' => [
                                                    'confirm' => '¿Está seguro de que desea borrar esta carpeta?',
                                                    'method' => 'post',
                                                ],
                                                    ]
                                    );
                                }
                            },
                                ],
                            ],
                        ],
                    ]);
                    ?>

                    <?php
                    if (isset($datosArchivos)) {
                        echo GridView::widget([
                            'dataProvider' => $datosArchivos,
                            'columns' => [
                                [
                                    'attribute' => $usuario->id,
                                    'header' => 'Archivo',
                                    'format' => 'raw',
                                    'value' => function ($model, $key, $index, $column) {
                                        /* @var $model app\models\logic\Documento */
                                        /* @var $column yii\grid\DataColumn */
                                        $rutaArchivo = $model->rutaArchivo;
                                        $rutaArchivo = Utils::decrypt($rutaArchivo);

                                        $idUsuario = $column->attribute;
                                        return Html::a($rutaArchivo, ['documentos/download', 'id' => $idUsuario, 'doc' => $model->idDocumento]);
                                    },
                                        ], [
                                            'attribute' => 'Usuario',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                /* @var $model app\models\logic\Documento */
                                                $idUsuario = $model->idUsuario;
                                                $usuarioActualizo = Usuario::findOne($idUsuario);
                                                return $usuarioActualizo->name;
                                            },
                                        ], [
                                            'class' => 'yii\grid\ActionColumn',
                                            'template' => '{delete}',
                                            'buttons' => [
                                                'delete' => function($url, $model) {
                                                    /* @var $model app\models\logic\Documento */
                                                    $idUsuarioL = Yii::$app->user->id;
                                                    $idUsuario = $model->idUsuario;

                                                    $categoria = $model->categoria;
                                                    $documentoPadre = $model->ficheroPadre;
                                                    $idFicheroPadre = null;
                                                    if (isset($documentoPadre)) {
                                                        $idFicheroPadre = $documentoPadre->idDocumento;
                                                    }

                                                    $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR, Constants::NUM_PROYECTO];
                                                    if ($idUsuario == $idUsuarioL && Utils::validarPermisosCategoria($roles, $categoria, Constants::ACCION_BORRAR_ARCHIVO) == 1) {
                                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                                                                    'delete-file',
                                                                    'doc' => $model->idDocumento,
                                                                    'c' => $categoria->idCategoria,
                                                                    'fp' => $idFicheroPadre
                                                                        ], [
                                                                    'class' => '',
                                                                    'data' => [
                                                                        'confirm' => '¿Está seguro de que desea borrar este archivo?',
                                                                        'method' => 'post',
                                                                    ],
                                                                        ]
                                                        );
                                                    }
                                                },
                                                    ],
                                                ],
                                            ],
                                        ]);
                                    }
                                    ?>

                                    <h1><?= Html::encode("Plantillas") ?></h1>

                                    <?=
                                    GridView::widget([
                                        'dataProvider' => $datosPlantillas,
                                        'columns' => [
                                            [
                                                'attribute' => $usuario->id,
                                                'header' => 'Archivo',
                                                'format' => 'raw',
                                                'value' => function ($model, $key, $index, $column) {
                                                    /* @var $model app\models\logic\Documento */
                                                    /* @var $column yii\grid\DataColumn */
                                                    $rutaArchivo = $model->rutaArchivo;
                                                    $rutaArchivo = Utils::decrypt($rutaArchivo);

                                                    $idUsuario = $column->attribute;
                                                    if ($model->esFichero == Constants::varNoBD) {
                                                        $url = Html::a($rutaArchivo, ['documentos/download', 'id' => $idUsuario, 'doc' => $model->idDocumento]);
                                                    }

                                                    return $url;
                                                },
                                                    ],
                                                    [
                                                        'attribute' => 'Usuario',
                                                        'format' => 'raw',
                                                        'value' => function ($model, $key, $index, $column) {
                                                            /* @var $model app\models\logic\Documento */
                                                            /* @var $column yii\grid\DataColumn */
                                                            $idUsuario = $model->idUsuario;
                                                            $usuarioActualizo = Usuario::findOne($idUsuario);

                                                            return $usuarioActualizo->name;
                                                        },
                                                    ],
                                                    [
                                                        'class' => 'yii\grid\ActionColumn',
                                                        'template' => '{delete}',
                                                        'buttons' => [
                                                            'delete' => function($url, $model) {
                                                                /* @var $model app\models\logic\Documento */
                                                                $idUsuarioL = Yii::$app->user->id;
                                                                $idUsuario = $model->idUsuario;

                                                                $categoria = $model->categoria;
                                                                $documentoPadre = $model->ficheroPadre;
                                                                $idFicheroPadre = null;
                                                                if (isset($documentoPadre)) {
                                                                    $idFicheroPadre = $documentoPadre->idDocumento;
                                                                }

                                                                $esFichero = $model->esFichero;
                                                                $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
                                                                if ($esFichero == Constants::varNoBD && $idUsuario == $idUsuarioL && Utils::validarPermisosCategoria($roles, $categoria, Constants::ACCION_BORRAR_PLANTILLA) == 1) {
                                                                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [
                                                                                'delete-file',
                                                                                'doc' => $model->idDocumento,
                                                                                'c' => $categoria->idCategoria,
                                                                                'fp' => $idFicheroPadre
                                                                                    ], [
                                                                                'class' => '',
                                                                                'data' => [
                                                                                    'confirm' => '¿Está seguro de que desea borrar esta plantilla?',
                                                                                    'method' => 'post',
                                                                                ],
                                                                                    ]
                                                                    );
                                                                }
                                                            }
                                                                ]
                                                            ],
                                                        ],
                                                    ]);
                                                    ?>

</div>
