<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Documento */
/* @var $usuario app\models\logic\Usuario */
/* @var $categoria app\models\logic\Categoria */
/* @var $documentoPadre app\models\logic\Documento */
?>
<?=

$this->render('_form-folder', [
    'model' => $model,
    'usuario' => $usuario,
    'categoria' => $categoria,
    'documentoPadre' => $documentoPadre,
])
?>
