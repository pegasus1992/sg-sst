<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\logic\Usuario;
use app\models\Constants;
use app\models\Utils;

/* @var $this yii\web\View */
/* @var $usuario app\models\logic\Usuario */
/* @var $categoriaPadre app\models\logic\Categoria */
/* @var $categoriasSearch app\models\search\CategoriasSearch */
/* @var $categoriasDataProvider yii\data\ActiveDataProvider */

$this->title = 'Documentos';
$this->params['breadcrumbs'][] = ['label' => $this->title, 'url' => ['documentos/', 'id' => Yii::$app->user->id]];
if ($usuario->id != Yii::$app->user->id) {
    $idL = Yii::$app->user->id;
    $usuarioL = Usuario::findOne($idL);
    $rolL = $usuarioL->authAssignments[0]->item_name;
    $rolI = $usuario->authAssignments[0]->item_name;
    $this->params['breadcrumbs'][] = ['label' => $usuario->name, 'url' => ['documentos/', 'id' => $usuario->id]];
}
$rutas = Utils::obtenerRutaCategorias($categoriaPadre);
for ($i = sizeof($rutas) - 1; $i > 1; $i--) {
    $this->params['breadcrumbs'][] = $rutas[$i]->nombre;
}
$this->params['breadcrumbs'][] = ['label' => $rutas[1]->nombre, 'url' => ['documentos/', 'id' => $usuario->id]];
$this->params['breadcrumbs'][] = ['label' => $categoriaPadre->nombre, 'url' => ['documentos/menu', 'id' => $usuario->id, 'c' => $categoriaPadre->idCategoria, 'fp' => null]];
?>
<div class="documento-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    $tiempo = Utils::mostrarTiempoRestante();
    if (isset($tiempo)) {
        Yii::$app->session->setFlash($tiempo['type'], $tiempo['message']);
    }
    ?>

    <?=
    GridView::widget([
        'dataProvider' => $categoriasDataProvider,
        'columns' => [
            [
                'attribute' => $usuario->id,
                'header' => 'Archivo',
                'format' => 'raw',
                'value' => function ($model, $key, $index, $column) {
                    /* @var $column yii\grid\DataColumn */
                    $idCategoria = $model->idCategoria;
                    $idUsuario = $column->attribute;
                    if ($model->tipo == Constants::tipoAuxiliar || $model->tipo == Constants::tipoCombinado) {
                        $url = Html::a($model->nombre, ['documentos/view', 'id' => $idUsuario, 'c' => $idCategoria, 'fp' => null]);
                    } else {
                        $url = $model->nombre;
                    }

                    switch ($model->nivel) {
                        case 1:
                            $url = "<b>$url</b>";
                            break;
                        default:
                            $url = "$url";
                            break;
                    }
                    return $url;
                },
                    ],
                    'formato',
                    'fechaDiligenciamiento',
                ],
            ]);
            ?>
</div>
