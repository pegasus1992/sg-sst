<?php
/* @var $this yii\web\View */

use app\models\User;
use app\models\logic\Usuario;
use app\models\search\CategoriasSearch;
use app\models\search\UsuariosSearch;
use yii\grid\GridView;
use yii\helpers\Html;
use app\models\logic\Empresa;
use app\models\Constants;

$this->title = 'Sistema de Gestión - Seguridad y Salud en el Trabajo';
?>
<div class="site-index">

    <div class="jumbotron">

        <?php
        if (!Yii::$app->user->isGuest) {
            $id = Yii::$app->user->id;
            $usuario = Usuario::findOne($id);
            $empresa = $usuario->empresa;
        } else {
            $empresa = Empresa::findOne(1);
        }
        ?>
        <h1>Sistema de Gestión</h1>

        <h2>Seguridad y Salud en el Trabajo</h2>

        <?php
        if (isset($empresa->rutaLogo)) {
            echo Html::img(Yii::$app->homeUrl . '/Logos/' . $empresa->rutaLogo, ['width' => '200px', 'height' => 'auto']);
        }
        if ($empresa->idEmpresa != 1) {
            echo '<p class="lead">' . $empresa->nombre . '</p>';
        }
        ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <!--p><a class="btn btn-default" href="http://www.yiiframework.com/doc/">Yii Documentation &raquo;</a></p-->
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <!--p><a class="btn btn-default" href="http://www.yiiframework.com/forum/">Yii Forum &raquo;</a></p-->
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <!--p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p-->
            </div>
        </div>

    </div>
</div>
