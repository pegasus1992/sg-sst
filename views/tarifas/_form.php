<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Tarifa */
/* @var $empresas[] app\models\logic\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifa-form">

    <?php
    $this->registerJsFile('@web/js/fileSelected.js');
    $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']]);
    ?>

    <?= $form->field($model, 'codigo')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput() ?>

    <?= $form->field($model, 'descripcion')->textInput() ?>

    <?php
    echo $form->field($model, 'rutaImagen')->begin();
    echo Html::activeLabel($model, 'rutaImagen');
    ?>
    <br>
    <label class="btn btn-primary">
        <?php
        echo Html::activeFileInput($model, 'rutaImagen', [
            'maxlength' => true,
            'size' => 60,
            'maxlength' => 250,
            'accept' => 'image/*',
            'class' => 'form-control-file',
            'style' => 'display: none;',
            'onchange' => "onFileSelected(event);",
        ]);
        ?>
        Buscar...
    </label>
    <?php
    $img = '<img id="logo" class="file-preview-image" style="width:100px;height:auto;max-width:100%;max-height:100%;">';
    if (!$model->isNewRecord) {
        if ($model->rutaImagen != "") {
            $img = Html::img(Yii::$app->homeUrl . '/Tarifas/' . $model->rutaImagen, ["id" => "logo", "class" => "file-preview-image", "style" => "width:100px;height:auto;max-width:100%;max-height:100%;"]);
        }
    }
    echo $img;
    echo Html::error($model, 'rutaImagen', ['class' => 'help-block']);
    echo $form->field($model, 'rutaImagen')->end();
    ?>

    <?= $form->field($model, 'valor')->input('number', ['min' => 0, 'pattern' => '^[0-9]']) ?>

    <?= $form->field($model, 'diasAplica')->input('number', ['min' => 0, 'pattern' => '^[0-9]']) ?>

    <?= $form->field($model, 'mesesAplica')->input('number', ['min' => 0, 'pattern' => '^[0-9]']) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
