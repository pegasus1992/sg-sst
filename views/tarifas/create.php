<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Tarifa */

$this->title = 'Registrar Tarifa';
$this->params['breadcrumbs'][] = ['label' => 'Tarifas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
