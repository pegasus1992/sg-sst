<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Constants;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Tarifa */

$this->title = "Ver Tarifa: $model->codigo";
$this->params['breadcrumbs'][] = ['label' => 'Tarifas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->codigo;
?>
<div class="tarifa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($model->estado == Constants::estadoActivoBD) {
            echo Html::a('Modificar', ['update', 'id' => $model->idTarifa], ['class' => 'btn btn-primary']);
            echo Html::a('Desactivar', ['delete', 'id' => $model->idTarifa], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿En verdad desea desactivar esta tarifa?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo',
            'nombre',
            'descripcion',
            [
                'attribute' => 'rutaImagen',
                'format' => 'raw',
                'value' => function ($model) {
                    if (!is_null($model->rutaImagen)) {
                        return Html::img(Yii::$app->homeUrl . '/Tarifas/' . $model->rutaImagen, ['width' => '100px', 'height' => 'auto']);
                    } else {
                        return '';
                    }
                },
                    ],
                    [
                        'attribute' => 'valor',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $valor = number_format($model->valor);
                            return "$ $valor";
                        },
                    ],
                    [
                        'attribute' => 'estado',
                        'format' => 'raw',
                        'value' => function ($model) {
                            /* @var $model app\models\logic\Pago */
                            switch ($model->estado) {
                                case Constants::estadoActivoBD:
                                    return Constants::estadoActivo;
                                default:
                                    return Constants::estadoInactivo;
                            }
                        },
                    ],
                ],
            ])
            ?>

</div>
