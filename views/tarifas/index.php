<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PagosSearch */
/* @var $tarifasActivas yii\data\ActiveDataProvider */
/* @var $tarifasInactivas yii\data\ActiveDataProvider */

$this->title = 'Tarifas actuales';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifa-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Tarifa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h2>Tarifas Activos</h2>

    <?=
    GridView::widget([
        'dataProvider' => $tarifasActivas,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'codigo',
            'nombre',
            [
                'attribute' => 'valor',
                'format' => 'raw',
                'value' => function ($model) {
                    $valor = number_format($model->valor);
                    return "$ $valor";
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{delete}',
            ],
        ],
    ]);
    ?>

    <h2>Tarifas Inactivas</h2>

    <?=
    GridView::widget([
        'dataProvider' => $tarifasInactivas,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'codigo',
            'nombre',
            [
                'attribute' => 'valor',
                'format' => 'raw',
                'value' => function ($model) {
                    $valor = number_format($model->valor);
                    return "$ $valor";
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);
    ?>
</div>
