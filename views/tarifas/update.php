<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Tarifa */

$this->title = "Modificar Tarifa: $model->codigo";
$this->params['breadcrumbs'][] = ['label' => 'Tarifas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->idTarifa]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="tarifa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
