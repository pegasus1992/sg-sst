<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */

$this->title = 'Crear Coordinador';
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['usuarios']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form-coordinador', [
        'model' => $model,
    ])
    ?>

</div>
