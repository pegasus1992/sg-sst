<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */
/* @var $empresas[] app\models\logic\Empresa */

$this->title = 'Crear Director';
$this->params['breadcrumbs'][] = ['label' => 'Directores', 'url' => ['directores']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form-director', [
        'model' => $model,
        'empresas' => $empresas,
    ])
    ?>

</div>
