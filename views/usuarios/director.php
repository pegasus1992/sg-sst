<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Directores', 'url' => ['directores']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update-director', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email',
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->empresa->nombre;
                },
            ],
            [
                'attribute' => 'empresa.rutaLogo',
                'format' => 'raw',
                'value' => function ($model) {
                    if (!is_null($model->empresa->rutaLogo)) {
                        return '<img src="' . Yii::$app->homeUrl . '/Logos/' . $model->empresa->rutaLogo . '" width="50px" height="auto">';
                    } else {
                        return '';
                    }
                },
            ],
        ],
    ])
    ?>

</div>
