<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Constants;
use app\models\Utils;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['usuarios']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        $rol = $model->authAssignments[0]->item_name;
        $url = null;
        if ($rol == Constants::ROL_COORDINADOR) {
            $url = 'update-coordinador';
        } else if ($rol == Constants::ROL_PROYECTO) {
            $url = 'update-proyecto';
        }
        if (isset($url)) {
            echo Html::a('Modificar', [$url, 'id' => $model->id], ['class' => 'btn btn-primary']);
        }
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email',
            [
                'attribute' => 'Rol',
                'format' => 'raw',
                'value' => function ($model) {
                    return Utils::getRol($model->authAssignments[0]->item_name);
                },
            ],
        ],
    ])
    ?>

</div>
