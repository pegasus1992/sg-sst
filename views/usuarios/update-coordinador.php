<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */

$this->title = "Modificar Coordinador: $model->name";
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['usuarios']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['usuario', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form-coordinador', [
        'model' => $model,
    ])
    ?>

</div>
