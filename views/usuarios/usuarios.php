<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Constants;
use app\models\Utils;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuarios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Coordinador', ['create-coordinador'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Crear Proyecto', ['create-proyecto'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email',
            [
                'attribute' => 'Rol',
                'format' => 'raw',
                'value' => function ($model) {
                    return Utils::getRol($model->authAssignments[0]->item_name);
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{restore}',
                'buttons' => [
                    'view' => function($url, $model) {
                        /* @var $model app\models\logic\Usuario */
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['usuario', 'id' => $model->id], ['class' => '']);
                    },
                            'update' => function($url, $model) {
                        /* @var $model app\models\logic\Usuario */
                        $rol = $model->authAssignments[0]->item_name;
                        $url = null;
                        if ($rol == Constants::ROL_COORDINADOR) {
                            $url = 'update-coordinador';
                        } else if ($rol == Constants::ROL_PROYECTO) {
                            $url = 'update-proyecto';
                        }
                        if (isset($url)) {
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$url, 'id' => $model->id], ['class' => '']);
                        }
                    },
                            'restore' => function($url, $model) {
                        /* @var $model app\models\logic\Usuario */
                        $rol = $model->authAssignments[0]->item_name;
                        $url = null;
                        if ($rol == Constants::ROL_COORDINADOR) {
                            $url = 'restore-coordinador';
                        } else if ($rol == Constants::ROL_PROYECTO) {
                            $url = 'restore-proyecto';
                        }
                        if (isset($url)) {
                            return Html::a('<span class="glyphicon glyphicon-edit"></span>', [$url, 'id' => $model->id], [
                                        'class' => '',
                                        'data' => [
                                            'confirm' => "¿Está seguro de que desea reestablecer la contraseña a $model->name?.",
                                            'method' => 'post',
                                        ],
                            ]);
                        }
                    },
                        ],
                    ]
                ],
            ]);
            ?>
</div>
