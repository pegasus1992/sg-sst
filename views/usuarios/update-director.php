<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */
/* @var $empresas mixed de Empresa */

$this->title = "Modificar Director: $model->name";
$this->params['breadcrumbs'][] = ['label' => 'Directores', 'url' => ['directores']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['director', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Modificar';
?>
<div class="usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form-director', [
        'model' => $model,
        'empresas' => $empresas,
    ])
    ?>

</div>
