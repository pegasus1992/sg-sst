<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UsuariosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Directores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Director', ['create-director'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email',
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->empresa->nombre;
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{update}{restore}',
                'buttons' => [
                    'view' => function($url, $model) {
                        /* @var $model app\models\logic\Usuario */
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['director', 'id' => $model->id], ['class' => '']);
                    },
                            'update' => function($url, $model) {
                        /* @var $model app\models\logic\Usuario */
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update-director', 'id' => $model->id], ['class' => '']);
                    },
                            'restore' => function($url, $model) {
                        /* @var $model app\models\logic\Usuario */
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>', ['restore-director', 'id' => $model->id], [
                                    'class' => '',
                                    'data' => [
                                        'confirm' => "¿Está seguro de que desea reestablecer la contraseña a $model->name?.",
                                        'method' => 'post',
                                    ],
                        ]);
                    },
                        ],
                    ]
                ],
            ]);
            ?>
</div>
