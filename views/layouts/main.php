<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\logic\Usuario;
use app\models\logic\Empresa;
use app\models\Constants;
use app\models\Utils;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            Yii::$app->name = "SG-SST";
            if (!Yii::$app->user->isGuest) {
                $id = Yii::$app->user->id;
                $usuario = Usuario::findOne($id);
                $empresa = $usuario->empresa;
            } else {
                $empresa = Empresa::findOne(1);
            }
            NavBar::begin([
                'brandLabel' => $empresa->nombre,
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);

            $id = Yii::$app->user->id;
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => [
                    ['label' => 'Inicio', 'url' => ['/']],
                    ['label' => 'Perfil', 'url' => ['/perfil'], 'visible' => !Yii::$app->user->isGuest],
                    ['label' => 'Empresas', 'url' => ['/empresas'], 'visible' => Utils::hasPermission(Constants::NUM_SUPERVISOR)],
                    ['label' => 'Directores', 'url' => ['/usuarios/directores'], 'visible' => Utils::hasPermission(Constants::NUM_SUPERVISOR)],
                    ['label' => 'Usuarios', 'url' => ['/usuarios/usuarios'], 'visible' => Utils::hasPermission(Constants::NUM_DIRECTOR)],
                    ['label' => 'Tarifas', 'url' => ['/tarifas/index'], 'visible' => Utils::hasPermission(Constants::NUM_SUPERVISOR)],
                    ['label' => 'Pagos', 'url' => ['/pagos/'], 'visible' => Utils::hasPermission(Constants::NUM_SUPERVISOR)],
                    ['label' => 'Tienda', 'url' => ['/pagos/ofertas/'], 'visible' => Utils::hasPermission(Constants::NUM_DIRECTOR)],
                    ['label' => 'Documentos', 'url' => ['/documentos/', 'id' => $id], 'visible' => !Yii::$app->user->isGuest],
//                    ['label' => 'About', 'url' => ['/site/about']],
//                    ['label' => 'Contact', 'url' => ['/site/contact']],
                    Yii::$app->user->isGuest ? (
                            ['label' => 'Login', 'url' => ['/site/login']]
                            ) : (
                            '<li>'
                            . Html::beginForm(['/site/logout'], 'post')
                            . Html::submitButton(
                                    'Logout (' . Yii::$app->user->identity->name . ')', ['class' => 'btn btn-link logout']
                            )
                            . Html::endForm()
                            . '</li>'
                            )
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?=
                Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ])
                ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

                <!--p class="pull-right"><?= Yii::powered() ?></p-->
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
