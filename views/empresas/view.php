<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Empresa */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar', ['update', 'id' => $model->idEmpresa], ['class' => 'btn btn-primary']) ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'nit',
            [
                'attribute' => 'rutaLogo',
                'format' => 'raw',
                'value' => function ($model) {
                    if (!is_null($model->rutaLogo)) {
                        return Html::img(Yii::$app->homeUrl . '/Logos/' . $model->rutaLogo, ['width' => '100px', 'height' => 'auto']);
                    } else {
                        return '';
                    }
                },
            ],
        ],
    ])
    ?>

</div>
