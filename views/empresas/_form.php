<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Empresa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-form">

    <?php
    $this->registerJsFile('@web/js/fileSelected.js');
    $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']]);
    ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nit')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'rutaLogo')->begin();
    echo Html::activeLabel($model, 'rutaLogo');
    ?>
    <br>
    <label class="btn btn-primary">
        <?php
        echo Html::activeFileInput($model, 'rutaLogo', [
            'maxlength' => true,
            'size' => 60,
            'maxlength' => 250,
            'accept' => 'image/*',
            'class' => 'form-control-file',
            'style' => 'display: none;',
            'onchange' => "onFileSelected(event);",
        ]);
        ?>
        Buscar...
    </label>
    <?php
    $img = '<img id="logo" class="file-preview-image" style="width:100px;height:auto;max-width:100%;max-height:100%;">';
    if (!$model->isNewRecord) {
        if ($model->rutaLogo != "") {
            $img = Html::img(Yii::$app->homeUrl . '/Logos/' . $model->rutaLogo, ["id" => "logo", "class" => "file-preview-image", "style" => "width:100px;height:auto;max-width:100%;max-height:100%;"]);
        }
    }
    echo $img;
    echo Html::error($model, 'rutaLogo', ['class' => 'help-block']);
    echo $form->field($model, 'rutaLogo')->end();
    ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
