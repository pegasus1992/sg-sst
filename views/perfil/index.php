<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;
use app\models\Constants;
use app\models\Utils;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */

$this->title = "Ver Perfil";
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Modificar Perfil', ['update'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Cambiar Contraseña', ['change'], ['class' => 'btn btn-warning']) ?>
        <?=
        Utils::hasPermission(Constants::ROL_SUPERVISOR) || Utils::hasPermission(Constants::ROL_DIRECTOR) ? Html::a('Modificar Empresa', ['empresa'], ['class' => 'btn btn-success']) : ''
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'email',
            [
                'attribute' => 'Rol',
                'format' => 'raw',
                'value' => function ($model) {
                    return Utils::getRol($model->authAssignments[0]->item_name);
                },
            ],
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->empresa->nombre;
                },
            ],
            [
                'attribute' => 'empresa.rutaLogo',
                'format' => 'raw',
                'value' => function ($model) {
                    if (!is_null($model->empresa->rutaLogo)) {
                        return Html::img(Yii::$app->homeUrl . '/Logos/' . $model->empresa->rutaLogo, ['width' => '100px', 'height' => 'auto']);
                    } else {
                        return '';
                    }
                },
                    ],
                ],
            ])
            ?>

</div>
