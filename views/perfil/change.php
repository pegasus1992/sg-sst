<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Perfil */
/* @var $form yii\widgets\ActiveForm */

$this->title = "Cambiar Contraseña: $model->name";
$this->params['breadcrumbs'][] = ['label' => "Ver Perfil", 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Cambiar Contraseña';
?>
<div class="perfil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="perfil-form">

        <?php
        $form = ActiveForm::begin();
        ?>

        <?= $form->field($model, 'oldpassword')->passwordInput(['maxlength' => true]) ?>

        <?=
                $form->field($model, 'newpassword')->passwordInput(['maxlength' => true, 'pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}'])
                ->hint("Debe contener al menos un número y una letra mayúscula y minúscula, con al menos 8 caracteres.")
        ?>

        <?= $form->field($model, 'repassword')->passwordInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
