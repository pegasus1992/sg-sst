<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */
/* @var $empresa app\models\logic\Empresa */
/* @var $form yii\widgets\ActiveForm */

$this->title = "Modificar Empresa: $empresa->nombre";
$this->params['breadcrumbs'][] = ['label' => "Ver Perfil", 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar Empresa';
?>
<div class="perfil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="perfil-form">

        <?php
        $this->registerJsFile('@web/js/fileSelected.js');
        $form = ActiveForm::begin([
                    'options' => ['enctype' => 'multipart/form-data']]);
        ?>

        <?= $form->field($empresa, 'nombre')->textInput(['maxlength' => true]) ?>

        <?= $form->field($empresa, 'nit')->textInput(['maxlength' => true]) ?>

        <?php
        echo $form->field($empresa, 'rutaLogo')->begin();
        echo Html::activeLabel($empresa, 'rutaLogo');
        ?>
        <br>
        <label class="btn btn-primary">
            <?php
            echo Html::activeFileInput($empresa, 'rutaLogo', [
                'maxlength' => true,
                'size' => 60,
                'maxlength' => 250,
                'accept' => 'image/*',
                'class' => 'form-control-file',
                'style' => 'display: none;',
                'onchange' => "onFileSelected(event);",
            ]);
            ?>
            Buscar...
        </label>
        <?php
        $img = '<img id="logo" class="file-preview-image" style="width:100px;height:auto;max-width:100%;max-height:100%;">';
        if (!$empresa->isNewRecord) {
            if ($empresa->rutaLogo != "") {
                $img = Html::img(Yii::$app->homeUrl . '/Logos/' . $empresa->rutaLogo, ["id" => "logo", "class" => "file-preview-image", "style" => "width:100px;height:auto;max-width:100%;max-height:100%;"]);
            }
        }
        echo $img;
        echo Html::error($empresa, 'rutaLogo', ['class' => 'help-block']);
        echo $form->field($empresa, 'rutaLogo')->end();
        ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
