<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Constants;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Usuario */
/* @var $form yii\widgets\ActiveForm */

$this->title = "Modificar Usuario: $model->name";
$this->params['breadcrumbs'][] = ['label' => "Ver Perfil", 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Modificar Perfil';
?>
<div class="perfil-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="perfil-form">

        <?php
        $rol = $model->authAssignments[0]->item_name;
        $form = ActiveForm::begin();
        ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?php
        $pattern = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
        if ($rol == Constants::ROL_COORDINADOR || $rol == Constants::ROL_PROYECTO) {
            echo $form->field($model, 'email')->input('email', [
                'maxlength' => true,
                'pattern' => $pattern,
                'disabled' => '',
            ]);
        } else {
            echo $form->field($model, 'email')->input('email', [
                'maxlength' => true,
                'pattern' => $pattern,
            ]);
        }
        ?>

        <div class="form-group">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
