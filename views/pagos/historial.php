<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PagosSearch */
/* @var $empresa \app\models\logic\Empresa */
/* @var $pagos yii\data\ActiveDataProvider */

$this->title = 'Historial de Pagos';
$this->params['breadcrumbs'][] = ['label' => 'Tienda', 'url' => ['ofertas']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pago-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    GridView::widget([
        'dataProvider' => $pagos,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'idPago',
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    return $model->empresa->nombre;
                },
            ],
            [
                'attribute' => 'fechaInicial',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaInicial);
                    return $fecha->format('d M Y');
                },
            ],
            [
                'attribute' => 'fechaFinal',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaFinal);
                    return $fecha->format('d M Y');
                },
            ],
            [
                'attribute' => 'idTarifa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $tarifa = $model->tarifa;
                    return "$tarifa->nombre";
                },
            ],
            [
                'attribute' => 'Valor Total',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $valor = number_format($model->getTotal());
                    return "$ $valor";
                },
            ],
        ],
    ]);
    ?>
</div>
