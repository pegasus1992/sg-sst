<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Tarifa */

$this->title = "Oferta seleccionada: $model->nombre";
$this->params['breadcrumbs'][] = ['label' => 'Tienda', 'url' => ['ofertas']];
$this->params['breadcrumbs'][] = $model->nombre;
?>
<div class="oferta-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'descripcion',
            [
                'attribute' => 'rutaImagen',
                'format' => 'raw',
                'value' => function ($model) {
                    if (!is_null($model->rutaImagen)) {
                        return Html::img(Yii::$app->homeUrl . '/Tarifas/' . $model->rutaImagen, ['width' => '100px', 'height' => 'auto']);
                    } else {
                        return '';
                    }
                },
                    ],
                    [
                        'attribute' => 'valor',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $valor = number_format($model->valor);
                            return "$ $valor";
                        },
                    ],
                ],
            ])
            ?>

            <?=
            Html::a('<span class="btn btn-success">Pagar</span>', ['pagar', 'id' => $model->idTarifa], [
                'class' => '',
                'data' => [
                    'confirm' => "¿Está seguro de que desea efectuar el pago de esta oferta?.",
                    'method' => 'post',
                ]
            ]);
            ?>

</div>
