<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PagosSearch */
/* @var $pagosActivos yii\data\ActiveDataProvider */
/* @var $pagosAnulados yii\data\ActiveDataProvider */

$this->title = 'Pagos realizados';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pago-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Registrar Pago', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <h2>Pagos Activos</h2>

    <?=
    GridView::widget([
        'dataProvider' => $pagosActivos,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'idPago',
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    return $model->empresa->nombre;
                },
            ],
            [
                'attribute' => 'fechaInicial',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaInicial);
                    return $fecha->format('d M Y');
                },
            ],
            [
                'attribute' => 'fechaFinal',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaFinal);
                    return $fecha->format('d M Y');
                },
            ],
            [
                'attribute' => 'idTarifa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $tarifa = $model->tarifa;
                    return "$tarifa->nombre";
                },
            ],
            [
                'attribute' => 'Valor Total',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $valor = number_format($model->getTotal());
                    return "$ $valor";
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}{delete}',
            ],
        ],
    ]);
    ?>

    <h2>Pagos Anulados</h2>

    <?=
    GridView::widget([
        'dataProvider' => $pagosAnulados,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'idPago',
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    return $model->empresa->nombre;
                },
            ],
            [
                'attribute' => 'fechaInicial',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaInicial);
                    return $fecha->format('d M Y');
                },
            ],
            [
                'attribute' => 'fechaFinal',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaFinal);
                    return $fecha->format('d M Y');
                },
            ],
            [
                'attribute' => 'idTarifa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $tarifa = $model->tarifa;
                    return "$tarifa->nombre";
                },
            ],
            [
                'attribute' => 'Valor Total',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $valor = number_format($model->getTotal());
                    return "$ $valor";
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]);
    ?>
</div>
