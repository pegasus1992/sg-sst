<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Constants;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Pago */

$this->title = "Ver Pago #$model->idPago";
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->idPago;
?>
<div class="pago-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if ($model->estado == Constants::estadoActivoBD) {
            echo Html::a('Anular', ['delete', 'id' => $model->idPago], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿En verdad desea anular este pago?',
                    'method' => 'post',
                ],
            ]);
        }
        ?>
    </p>

    <?=
    DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idPago',
            [
                'attribute' => 'idEmpresa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    return $model->empresa->nombre;
                },
            ],
            [
                'attribute' => 'fechaInicial',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaInicial);
                    return $fecha->format('d M Y H:i:s');
                },
            ],
            [
                'attribute' => 'fechaFinal',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $fecha = new DateTime($model->fechaFinal);
                    return $fecha->format('d M Y H:i:s');
                },
            ],
            [
                'attribute' => 'idTarifa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $tarifa = $model->tarifa;
                    return "$tarifa->nombre";
                },
            ],
            [
                'attribute' => 'Valor Tarifa',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $tarifa = $model->tarifa;
                    $valor = number_format($tarifa->valor);
                    return "$ $valor";
                },
            ],
            [
                'attribute' => 'valorDescuento',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $valor = number_format($model->valorDescuento);
                    return "$ $valor";
                },
            ],
            [
                'attribute' => 'I V A',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $subtotal = $model->getTotalSinIVA();
                    $porcIva = $model->porcentajeIva;
                    $valor = number_format($subtotal * $porcIva / 100);
                    return "$ $valor ($porcIva%)";
                },
            ],
            [
                'attribute' => 'Total',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    $valor = number_format($model->getTotal());
                    return "$ $valor";
                },
            ],
            [
                'attribute' => 'estado',
                'format' => 'raw',
                'value' => function ($model) {
                    /* @var $model app\models\logic\Pago */
                    switch ($model->estado) {
                        case Constants::estadoActivoBD:
                            return Constants::estadoActivo;
                        default:
                            return Constants::estadoAnulado;
                    }
                },
            ],
        ],
    ])
    ?>

</div>
