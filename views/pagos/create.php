<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Pago */
/* @var $empresas[] app\models\logic\Empresa */
/* @var $tarifas[] app\models\logic\Tarifa */

$this->title = 'Registrar Pago';
$this->params['breadcrumbs'][] = ['label' => 'Pagos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pago-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'empresas' => $empresas,
        'tarifas' => $tarifas,
    ])
    ?>

</div>
