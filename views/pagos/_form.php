<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\logic\Pago */
/* @var $empresas[] app\models\logic\Empresa */
/* @var $tarifas[] app\models\logic\Tarifa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pago-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idEmpresa')->dropDownList($empresas, ['prompt' => 'Seleccione una empresa...']) ?>
    
    <?= $form->field($model, 'idTarifa')->dropDownList($tarifas, ['prompt' => 'Seleccione una tarifa...']) ?>

    <?= $form->field($model, 'porcentajeIva')->textInput() ?>

    <?= $form->field($model, 'valorDescuento')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
