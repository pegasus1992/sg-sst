<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PagosSearch */
/* @var $tarifas yii\data\ActiveDataProvider */

$this->title = 'Tienda';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="oferta-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Historial de Pagos', ['historial'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $tarifas,
        'filterModel' => $searchModel,
        'columns' => [
            'nombre',
            [
                'attribute' => 'rutaImagen',
                'format' => 'raw',
                'value' => function ($model) {
                    if (!is_null($model->rutaImagen)) {
                        return Html::img(Yii::$app->homeUrl . '/Tarifas/' . $model->rutaImagen, ['width' => '100px', 'height' => 'auto']);
                    } else {
                        return '';
                    }
                },
                    ],
                    [
                        'attribute' => 'valor',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $valor = number_format($model->valor);
                            return "$ $valor";
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{pagar}',
                        'buttons' => [
                            'pagar' => function($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['oferta', 'id' => $model->idTarifa], ['class' => '',]);
                            }
                                ],
                            ],
                        ],
                    ]);
                    ?>
</div>
