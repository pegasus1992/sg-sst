<?php

namespace app\models;

use Yii;
use app\models\logic\Usuario;
use app\models\logic\Categoria;
use app\models\logic\Documento;
use app\models\search\PagosSearch;

/**
 * Description of Utils
 *
 * @author Avuunita
 */
class Utils {

    /**
     * Permite encriptar una cadena dada.
     * @param type $string
     * @return type
     */
    public static function encrypt($string) {
        return Yii::$app->encrypter->encrypt($string);
    }

    /**
     * Permite desencriptar una cadena dada.
     * @param type $string
     * @return type
     */
    public static function decrypt($string) {
        return Yii::$app->encrypter->decrypt($string);
    }

    /**
     * Obtiene el nombre concreto del rol almacenado en la BD.
     * @param string $rol
     * @return string
     */
    public static function getRol($rol) {
        switch ($rol) {
            case Constants::ROL_SUPERVISOR:
            case Constants::NUM_SUPERVISOR:
                return "Supervisor";
            case Constants::ROL_DIRECTOR:
            case Constants::NUM_DIRECTOR:
                return "Director";
            case Constants::ROL_COORDINADOR:
            case Constants::NUM_COORDINADOR:
                return "Coordinador";
            case Constants::ROL_PROYECTO:
            case Constants::NUM_PROYECTO:
                return "Proyecto";
        }
        return null;
    }

    private static function traducirRol($rol) {
        switch ($rol) {
            case Constants::ROL_SUPERVISOR:
                return Constants::NUM_SUPERVISOR;
            case Constants::NUM_SUPERVISOR:
                return Constants::ROL_SUPERVISOR;
            case Constants::ROL_DIRECTOR:
                return Constants::NUM_DIRECTOR;
            case Constants::NUM_DIRECTOR:
                return Constants::ROL_DIRECTOR;
            case Constants::ROL_COORDINADOR:
                return Constants::NUM_COORDINADOR;
            case Constants::NUM_COORDINADOR:
                return Constants::ROL_COORDINADOR;
            case Constants::ROL_PROYECTO:
                return Constants::NUM_PROYECTO;
            case Constants::NUM_PROYECTO:
                return Constants::ROL_PROYECTO;
        }
        return null;
    }

    /**
     * Verifica si el usuario logeado tiene permisos como el rol ingresado.
     * @param string $rol
     * @return type
     */
    public static function hasPermission($rol) {
//        $id = Yii::$app->user->id;
//        return !Yii::$app->user->isGuest && Usuario::findOne($id)->authAssignments[0]->item_name == $rol;
        return !Yii::$app->user->isGuest && Yii::$app->user->can($rol);
    }

    public static function generateTimeMark() {
        $now = new \DateTime();
        $year = $now->format('Y');
        $month = $now->format('m');
        $day = $now->format('d');
        $hour24 = $now->format('H');
        $minute = $now->format('i');
        $second = $now->format('s');

        $year2seconds = $year * 12 * 30 * 24 * 60 * 60;
        $month2seconds = $month * 30 * 24 * 60 * 60;
        $day2seconds = $day * 24 * 60 * 60;
        $hour2seconds = $hour24 * 60 * 60;
        $minute2seconds = $minute * 60;

        $time = $second + $minute2seconds + $hour2seconds + $day2seconds + $month2seconds + $year2seconds;
        return $time;
    }

    public static function rand_string($length) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        return substr(str_shuffle($chars), 0, $length);
    }

    public static function rand_pass() {
        return self::rand_string(8);
    }

    /**
     * Obtiene la ruta completa de una categoria (del ultimo hacia el primero).
     * @param Categoria $categoria
     * @return Categoria[]
     */
    public static function obtenerRutaCategorias($categoria) {
        $ruta = [];
        $cat = $categoria;
        while (isset($cat)) {
            $ruta[] = $cat;
            $cat = $cat->categoriaPadre;
        }
        return $ruta;
    }

    /**
     * Obtiene la ruta completa de un fichero, dentro de una categoria (del ultimo hacia el primero).
     * @param Documento $documento
     * @return Documento[]
     */
    public static function obtenerRutaFicheros($documento) {
        $carpetas = [];
        $doc = $documento;
        while (isset($doc)) {
            $carpetas[] = $doc;
            $doc = $doc->ficheroPadre;
        }
        return $carpetas;
    }

    /**
     * Verifica si el usuario logeado tiene permiso para hacer una determinada accion dentro de una categoria.
     * @param string[] $roles
     * @param Categoria $categoria
     * @param int $accion
     * @return string
     */
    public static function validarPermisosCategoria($roles, $categoria, $accion) {
        $rolValido = false;
        if (isset($roles)) {
            foreach ($roles as $rol) {
                $rolValido = $rolValido || self::hasPermission($rol);
            }
        }
        if (!$rolValido) {
            return "ROL";
        }

        if (!isset($accion)) {
            return "ACCION";
        }

        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $rol = $usuario->authAssignments[0]->item_name;

        /* @var $permisos[] logic\Permiso */
        $permisos = $categoria->permisos;
        $permiso = null;
        foreach ($permisos as $p) {
            if ($p->rol == $rol && $p->idCategoria == $categoria->idCategoria) {
                $permiso = $p;
                break;
            }
        }

        if (!isset($permiso)) {
            return "PERMISO";
        }
        switch ($accion) {
            case Constants::ACCION_CREAR_CARPETA:
                return $permiso->crearCarpeta;
            case Constants::ACCION_BORRAR_CARPETA:
                return $permiso->borrarCarpeta;
            case Constants::ACCION_SUBIR_ARCHIVO:
                return $permiso->subirArchivo;
            case Constants::ACCION_BORRAR_ARCHIVO:
                return $permiso->borrarArchivo;
            case Constants::ACCION_SUBIR_PLANTILLA:
                return $permiso->subirPlantilla;
            case Constants::ACCION_BORRAR_PLANTILLA:
                return $permiso->borrarPlantilla;
            default:
                return "ACCION2";
        }
    }

    /**
     * Muestra el tiempo restante en pantalla.
     * @return string[]|null
     */
    public static function mostrarTiempoRestante() {
        $params = Yii::$app->request->queryParams;
        $idL = Yii::$app->user->id;
        if (isset($idL)) {
            $usuarioL = Usuario::findOne($idL);
            $empresaL = $usuarioL->empresa;
            $rolL = $usuarioL->authAssignments[0]->item_name;

            if ($empresaL->idEmpresa != 1 && $rolL == Constants::ROL_DIRECTOR) {
                $pagosSearch = new PagosSearch();
                $tiempo = $pagosSearch->traerTiempoRestante($empresaL, $params);

                $msg = "";
                $type = "";
                if ($tiempo === 0) {
                    $msg = "Su tiempo de trabajo ha terminado.";
                    $type = "danger";
                } else {
                    $anios = $tiempo->format('%y');
                    $meses = $tiempo->format('%m');
                    $dias = $tiempo->format('%d');
                    $horas = $tiempo->format('%h');
                    $minutos = $tiempo->format('%i');
                    $segundos = $tiempo->format('%s');

                    $msg = "Le quedan ";
                    if ($anios > 0) {
                        if ($anios > 1) {
                            $msg .= "$anios años";
                        } else {
                            $msg .= "$anios año";
                        }
                        $msg .= " ";
                    }
                    if ($meses > 0) {
                        if ($meses > 1) {
                            $msg .= "$meses meses";
                        } else {
                            $msg .= "$meses mes";
                        }
                        $msg .= " ";
                    }
                    if ($dias > 0) {
                        if ($dias > 1) {
                            $msg .= "$dias días";
                        } else {
                            $msg .= "$dias día";
                        }
                        $msg .= " ";
                    }
                    if ($horas > 0) {
                        if ($horas > 1) {
                            $msg .= "$horas horas";
                        } else {
                            $msg .= "$horas hora";
                        }
                        $msg .= " ";
                    }
                    if ($minutos > 0) {
                        if ($minutos > 1) {
                            $msg .= "$minutos minutos";
                        } else {
                            $msg .= "$minutos minuto";
                        }
                        $msg .= " ";
                    }
                    if ($segundos > 0) {
                        if ($segundos > 1) {
                            $msg .= "$segundos segundos";
                        } else {
                            $msg .= "$segundos segundo";
                        }
                    }
                    $msg .= " de tiempo de uso.";

                    $type = "success";
                    if ($anios == 0 && $meses == 0) {
                        if ($dias <= 5) {
                            if ($dias == 0 && $horas <= 1) {
                                $type = "danger";
                            } else {
                                $type = "warning";
                            }
                        }
                    }
                }

                return [
                    'type' => $type,
                    'message' => $msg,
                ];
            }
        }
        return null;
    }

}
