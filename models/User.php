<?php

namespace app\models;

use app\models\logic\Usuario;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface {

    public $id;
    public $name;
    public $password;
    public $email;
    public $authKey;
    public $accessToken;
    public $idEmpresa;
    public $idUsuarioRegistro;
    public $idUsuarioActualizo;
    public $accionUsuario;
    public $fechaCreacion;
    public $fechaModificacion;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        $user = Usuario::find()
                ->where("id = :id", ["id" => $id])
                ->one();
        return isset($user) ? new static($user) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null) {
        $users = Usuario::find()
                ->where("accessToken = :accessToken", ["accessToken" => $token])
                ->all();

        foreach ($users as $user) {
            if ($user->accessToken === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email) {
        $users = Usuario::find()
                ->where("email = :email", ["email" => $email])
                ->all();

        foreach ($users as $user) {
            if (strcasecmp($user->email, $email) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        if (sha1($password) == $this->password) {
            return $password === $password;
        }
    }

}
