<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\logic\Empresa;
use app\models\logic\Tarifa;
use app\models\logic\Pago;
use app\models\Constants;

/**
 * PagosSearch represents the model behind the search form of `app\models\logic\Pago`.
 */
class PagosSearch extends Pago {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idPago', 'idEmpresa', 'idTarifa', 'idUsuarioRegistro', 'idUsuarioActualizo'], 'integer'],
            [['fechaInicial', 'fechaFinal', 'estado', 'accionUsuario', 'fechaCreacion', 'fechaModificacion'], 'safe'],
            [['porcentajeIva', 'valorDescuento'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     * @param type $estado
     * @param type $params
     * @return ActiveDataProvider
     */
    public function buscar($estado, $params) {
        $query = Pago::find()
                ->select("*")
                ->from("Pago")
                ->where("idEmpresa <> 1");

        switch ($estado) {
            case Constants::estadoActivo:
            case Constants::estadoActivoBD:
                $query->andWhere("estado LIKE '" . Constants::estadoActivoBD . "'");
                break;
            case Constants::estadoAnulado:
            case Constants::estadoAnuladoBD:
                $query->andWhere("estado LIKE '" . Constants::estadoAnuladoBD . "'");
                break;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idPago' => $this->idPago,
            'idEmpresa' => $this->idEmpresa,
            'fechaInicial' => $this->fechaInicial,
            'fechaFinal' => $this->fechaFinal,
            'idTarifa' => $this->idTarifa,
            'porcentajeIva' => $this->porcentajeIva,
            'valorDescuento' => $this->valorDescuento,
            'idUsuarioRegistro' => $this->idUsuarioRegistro,
            'idUsuarioActualizo' => $this->idUsuarioActualizo,
            'fechaCreacion' => $this->fechaCreacion,
            'fechaModificacion' => $this->fechaModificacion,
        ]);

        $query->andFilterWhere(['like', 'accionUsuario', $this->accionUsuario]);

        $query->orderBy("fechaModificacion DESC");

        return $dataProvider;
    }

    /**
     * Obtiene el estado de cuenta actual de una empresa.
     * @param Empresa $empresa
     * @param type $params
     * @return ActiveDataProvider
     */
    public function buscarEstadoCuenta($empresa, $params) {
        $query = Pago::find()
                ->select("*")
                ->from("Pago P")
                ->leftJoin("Empresa E", "P.idEmpresa = E.idEmpresa")
                ->leftJoin("Tarifa T", "P.idTarifa = T.idTarifa")
                ->where("P.estado LIKE '" . Constants::estadoActivoBD . "'")
                ->andWhere("E.idEmpresa = $empresa->idEmpresa")
                ->andWhere("( NOW() BETWEEN P.fechaInicial AND P.fechaFinal OR P.fechaFinal >= NOW() )")
                ->orderBy("P.fechaCreacion");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Obtiene el periodo de trabajo por el que ha pagado una empresa, dada una tarifa.
     * @param Empresa $empresa
     * @param Tarifa $tarifa
     * @param type $params
     * @return string[]
     */
    public function traerFechasPago($empresa, $tarifa, $params) {
        $fechaInicial = new \DateTime();

        $pagosSearch = new PagosSearch();
        $pagos = $pagosSearch->buscarEstadoCuenta($empresa, $params)->getModels();
        if (isset($pagos) && sizeof($pagos) > 0) {
            $max = new \DateTime();
            foreach ($pagos as $pago) {
                /* @var $pago Pago */
                $fecha = new \DateTime($pago->fechaFinal);
                if ($fecha > $max) {
                    $max = $fecha;
                }
            }
            $fechaInicial = $max;
        }

        $fechaFinal = new \DateTime($fechaInicial->format('Y-m-d H:i:s'));
        $fechaFinal->add(new \DateInterval('P' . $tarifa->mesesAplica . 'M' . $tarifa->diasAplica . 'D'));

        $resp = [
            'fechaInicial' => $fechaInicial,
            'fechaFinal' => $fechaFinal,
        ];
        return $resp;
    }

    /**
     * Obtiene el tiempo restante que le queda a una empresa.
     * @param Empresa $empresa
     * @param type $params
     * @return type
     */
    public function traerTiempoRestante($empresa, $params) {
        $tiempo = 0;

        $pagosSearch = new PagosSearch();
        $pagos = $pagosSearch->buscarEstadoCuenta($empresa, $params)->getModels();
        if (isset($pagos) && sizeof($pagos) > 0) {
            $max = new \DateTime();
            foreach ($pagos as $pago) {
                /* @var $pago Pago */
                $fecha = new \DateTime($pago->fechaFinal);
                if ($fecha > $max) {
                    $max = $fecha;
                }
            }

            $hoy = new \DateTime();
            $tiempo = $max->diff($hoy);
        }

        return $tiempo;
    }

    /**
     * Obtiene el historial de pagos de una empresa.
     * @param Empresa $empresa
     * @param type $params
     * @return ActiveDataProvider
     */
    public function buscarHistorialPagos($empresa, $params) {
        $query = Pago::find()
                ->select("*")
                ->from("Pago P")
                ->leftJoin("Empresa E", "P.idEmpresa = E.idEmpresa")
                ->leftJoin("Tarifa T", "P.idTarifa = T.idTarifa")
                ->where("P.estado LIKE '" . Constants::estadoActivoBD . "'")
                ->andWhere("E.idEmpresa = $empresa->idEmpresa")
                ->orderBy("P.fechaCreacion DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}
