<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\logic\Tarifa;
use app\models\Constants;

/**
 * TarifasSearch represents the model behind the search form of `app\models\logic\Tarifa`.
 */
class TarifasSearch extends Tarifa {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idTarifa', 'diasAplica', 'mesesAplica'], 'integer'],
            [['codigo', 'nombre', 'estado'], 'safe'],
            [['valor'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Busca todas las tarifas activas, listas para ser aplicadas.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function buscarTarifas($estado, $params) {
        $query = Tarifa::find()
                ->select("*")
                ->from("Tarifa");

        switch ($estado) {
            case Constants::estadoActivo:
            case Constants::estadoActivoBD:
                $query->where("estado LIKE '" . Constants::estadoActivoBD . "'");
                break;
            case Constants::estadoInactivo:
            case Constants::estadoInactivoBD:
                $query->where("estado LIKE '" . Constants::estadoInactivoBD . "'");
                break;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idTarifa' => $this->idTarifa,
            'valor' => $this->valor,
            'diasAplica' => $this->diasAplica,
            'mesesAplica' => $this->mesesAplica,
        ]);

        $query->andFilterWhere(['like', 'codigo', $this->codigo])
                ->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'estado', $this->estado]);

        $query->orderBy("codigo");

        return $dataProvider;
    }

    /**
     * Busca todas las ofertas, listas para ser aplicadas.
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function buscarOfertas($params) {
        $query = Tarifa::find()
                ->select("*")
                ->from("Tarifa")
                ->where("estado LIKE '" . Constants::estadoActivoBD . "'")
                ->andWhere("valor > 0.0");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->orderBy("codigo");

        return $dataProvider;
    }

}
