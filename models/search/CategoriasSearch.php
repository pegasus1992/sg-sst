<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Constants;
use app\models\logic\Categoria;

/**
 * CategoriasSearch represents the model behind the search form of `app\models\logic\Categoria`.
 */
class CategoriasSearch extends Categoria {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['idCategoria', 'idCategoriaPadre', 'nivel'], 'integer'],
            [['nombre', 'formato', 'fechaDiligenciamiento', 'tipo', 'soloArchivos'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchMenu($params) {
        $query = Categoria::find()
                ->select("*")
                ->from("Categoria")
                ->where("tipo IN ('" . Constants::tipoMayor . "', '" . Constants::tipoAuxiliar . "')");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idCategoria' => $this->idCategoria,
            'idCategoriaPadre' => $this->idCategoriaPadre,
            'nivel' => $this->nivel,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'formato', $this->formato])
                ->andFilterWhere(['like', 'fechaDiligenciamiento', $this->fechaDiligenciamiento])
                ->andFilterWhere(['like', 'tipo', $this->tipo])
                ->andFilterWhere(['like', 'soloArchivos', $this->soloArchivos]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchSubmenu($idCategoriaPadre, $params) {
        $query = Categoria::find()
                ->select("*")
                ->from("Categoria")
                ->where("tipo IN ('" . Constants::tipoAuxiliar . "', '" . Constants::tipoCombinado . "')")
                ->andWhere("idCategoriaPadre = $idCategoriaPadre");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idCategoria' => $this->idCategoria,
            'idCategoriaPadre' => $this->idCategoriaPadre,
            'nivel' => $this->nivel,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'formato', $this->formato])
                ->andFilterWhere(['like', 'fechaDiligenciamiento', $this->fechaDiligenciamiento])
                ->andFilterWhere(['like', 'tipo', $this->tipo])
                ->andFilterWhere(['like', 'soloArchivos', $this->soloArchivos]);

        return $dataProvider;
    }

}
