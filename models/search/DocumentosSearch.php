<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\logic\Documento;
use app\models\logic\Categoria;
use app\models\logic\Usuario;
use app\models\User;
use app\models\Constants;

/**
 * DocumentosSearch represents the model behind the search form of `app\models\logic\Documento`.
 */
class DocumentosSearch extends Documento {

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idDocumento', 'idUsuario', 'idCategoria', 'idFicheroPadre'], 'integer'],
            [['rutaArchivo', 'esFichero', 'esPlantilla', 'fechaRegistro'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * 
     * @param Categoria $categoria
     * @param Documento $ficheroPadre
     * @return type
     */
    private static function getQuery($categoria, $ficheroPadre = null) {
        $query = Documento::find()
                ->select("*")
                ->from("Documento D")
                ->leftJoin("Usuario U", "D.idUsuario = U.id")
                ->leftJoin("Categoria C", "D.idCategoria = C.idCategoria")
                ->leftJoin("auth_assignment A", "U.id = A.user_id")
                ->where("D.eliminado = '" . Constants::varNoBD . "'")
                ->andWhere("D.idCategoria = '$categoria->idCategoria'");
        if (isset($ficheroPadre)) {
            $query->andWhere("D.idFicheroPadre = '$ficheroPadre->idDocumento'");
        } else {
            $query->andWhere("D.idFicheroPadre IS NULL");
        }
        return $query;
    }

    /**
     * Busca todas las carpetas a las que tenga acceso un usuario, dentro de una categoria, dentro (o no) de una carpeta.
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param type $params
     * @param Documento $ficheroPadre
     * @return ActiveDataProvider
     */
    public function buscarCarpetas($usuario, $categoria, $params, $ficheroPadre = null) {
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        $query = self::getQuery($categoria, $ficheroPadre)
                ->andWhere("D.esFichero = '" . Constants::varSiBD . "'");

        switch ($rol) {
            case Constants::ROL_SUPERVISOR:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_SUPERVISOR . "')");
                break;
            case Constants::ROL_DIRECTOR:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_SUPERVISOR . "', '" . Constants::ROL_DIRECTOR . "', '" . Constants::ROL_COORDINADOR . "')");
                break;
            case Constants::ROL_COORDINADOR:
                $query->andWhere("( A.item_name IN ('" . Constants::ROL_SUPERVISOR . "', '" . Constants::ROL_DIRECTOR . "') "
                        . "OR (A.item_name IN ('" . Constants::ROL_COORDINADOR . "') AND U.id = $usuario->id) )");
                break;
            case Constants::ROL_PROYECTO:
                $query->andWhere("( A.item_name IN ('" . Constants::ROL_SUPERVISOR . "', '" . Constants::ROL_DIRECTOR . "') "
                        . "OR (A.item_name IN ('" . Constants::ROL_COORDINADOR . "') AND U.id = $usuario->idUsuarioRegistro) )");
                break;
        }

        if (!isset($empresa->idEmpresa)) {
            $query->andWhere("(U.idEmpresa = 1)");
        } else {
            $query->andWhere("(U.idEmpresa = 1 OR U.idEmpresa = '$empresa->idEmpresa')");
        }

        $query->orderBy("A.item_name, D.fechaRegistro DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Busca todas las plantillas a las que tenga acceso un usuario, dentro de una categoria, dentro (o no) de una carpeta.
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param type $params
     * @param Documento $ficheroPadre
     * @return ActiveDataProvider
     */
    public function buscarPlantillas($usuario, $categoria, $params, $ficheroPadre = null) {
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        $query = self::getQuery($categoria, $ficheroPadre)
                ->andWhere("D.esFichero = '" . Constants::varNoBD . "'")
                ->andWhere("D.esPlantilla = '" . Constants::varSiBD . "'");

        switch ($rol) {
            case Constants::ROL_SUPERVISOR:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_SUPERVISOR . "')");
                break;
            case Constants::ROL_DIRECTOR:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_SUPERVISOR . "', '" . Constants::ROL_DIRECTOR . "', '" . Constants::ROL_COORDINADOR . "')");
                break;
            case Constants::ROL_COORDINADOR:
                $query->andWhere("( A.item_name IN ('" . Constants::ROL_SUPERVISOR . "', '" . Constants::ROL_DIRECTOR . "') "
                        . "OR (A.item_name IN ('" . Constants::ROL_COORDINADOR . "') AND U.id = $usuario->id) )");
                break;
            case Constants::ROL_PROYECTO:
                $query->andWhere("( A.item_name IN ('" . Constants::ROL_SUPERVISOR . "', '" . Constants::ROL_DIRECTOR . "') "
                        . "OR (A.item_name IN ('" . Constants::ROL_COORDINADOR . "') AND U.id = $usuario->idUsuarioRegistro) )");
                break;
        }

        if (!isset($empresa->idEmpresa)) {
            $query->andWhere("(U.idEmpresa = 1)");
        } else {
            $query->andWhere("(U.idEmpresa = 1 OR U.idEmpresa = '$empresa->idEmpresa')");
        }

        $query->orderBy("A.item_name, D.fechaRegistro DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Busca todos los archivos a los que tenga acceso un usuario, dentro de una categoria, dentro (o no) de una carpeta.
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param type $params
     * @param Documento $ficheroPadre
     * @return ActiveDataProvider
     */
    public function buscarArchivos($usuario, $categoria, $params, $ficheroPadre = null) {
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        $query = self::getQuery($categoria, $ficheroPadre)
                ->andWhere("D.esFichero = '" . Constants::varNoBD . "'")
                ->andWhere("D.esPlantilla = '" . Constants::varNoBD . "'")
                ->andWhere("U.idEmpresa = '$empresa->idEmpresa'");

        switch ($rol) {
            case Constants::ROL_SUPERVISOR:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_SUPERVISOR . "')");
                break;
            case Constants::ROL_DIRECTOR:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_DIRECTOR . "', '" . Constants::ROL_COORDINADOR . "', '" . Constants::ROL_PROYECTO . "')");
                break;
            case Constants::ROL_COORDINADOR:
                $query->andWhere("( (A.item_name IN ('" . Constants::ROL_COORDINADOR . "') AND U.id = $usuario->id) "
                        . "OR (A.item_name IN ('" . Constants::ROL_PROYECTO . "') AND D.idUsuario IN "
                        . "( SELECT UU.id FROM Usuario UU "
                        . "LEFT JOIN auth_assignment AA ON (UU.id = AA.user_id) "
                        . "WHERE UU.idEmpresa = $empresa->idEmpresa "
                        . "AND AA.item_name IN ('" . Constants::ROL_PROYECTO . "') "
                        . "AND UU.idUsuarioRegistro = $usuario->id )"
                        . ") )");
                break;
            case Constants::ROL_PROYECTO:
                $query->andWhere("A.item_name IN ('" . Constants::ROL_PROYECTO . "') AND U.id = $usuario->id");
                break;
        }

        $query->orderBy("A.item_name DESC, D.fechaRegistro DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Verifica si una carpeta esta vacia de contenido.
     * @param Usuario $usuario
     * @param Documento $documento
     * @param type $params
     * @return boolean
     */
    public function validarContenidoCarpeta($usuario, $documento, $params) {
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        $query = Documento::find()
                ->select("*")
                ->from("Documento D")
                ->leftJoin("Usuario U", "D.idUsuario = U.id")
                ->leftJoin("Categoria C", "D.idCategoria = C.idCategoria")
                ->leftJoin("auth_assignment A", "U.id = A.user_id")
                ->where("D.eliminado = '" . Constants::varNoBD . "'")
                ->andWhere("D.idFicheroPadre = '$documento->idDocumento'");

        if ($rol == Constants::ROL_DIRECTOR || $rol == Constants::ROL_COORDINADOR) {
            $query->andWhere("U.idEmpresa = '$empresa->idEmpresa'");
        }

        $query->orderBy("A.item_name, D.fechaRegistro DESC");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        return isset($dataProvider->models) && sizeof($dataProvider->models) > 0;
    }

}
