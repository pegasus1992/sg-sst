<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\logic\Empresa;
use app\models\logic\Usuario;
use app\models\Constants;

/**
 * UsuariosSearch represents the model behind the search form of `app\models\logic\Usuario`.
 */
class UsuariosSearch extends Usuario {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id', 'idEmpresa', 'idUsuarioRegistro', 'idUsuarioActualizo'], 'integer'],
            [['name', 'username', 'password', 'email', 'accessToken', 'authKey', 'accionUsuario', 'fechaCreacion', 'fechaModificacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function buscarDirectores($params) {
        $query = Usuario::find()
                ->select("U.*, E.*, A.*")
                ->from("Usuario U")
                ->leftJoin("Empresa E", "U.idEmpresa = E.idEmpresa")
                ->leftJoin("auth_assignment A", "A.user_id = U.id")
                ->where("A.item_name IN ('" . Constants::ROL_DIRECTOR . "')");

        // grid filtering conditions
        $query->andFilterWhere([
            'U.id' => $this->id,
            'E.idEmpresa' => $this->idEmpresa,
            'U.idUsuarioActualizo' => $this->idUsuarioActualizo,
            'U.fechaCreacion' => $this->fechaCreacion,
            'U.fechaModificacion' => $this->fechaModificacion,
        ]);

        $query->andFilterWhere(['like', 'U.name', $this->name])
                ->andFilterWhere(['like', 'U.password', $this->password])
                ->andFilterWhere(['like', 'U.email', $this->email])
                ->andFilterWhere(['like', 'U.accessToken', $this->accessToken])
                ->andFilterWhere(['like', 'U.authKey', $this->authKey])
                ->andFilterWhere(['like', 'U.accionUsuario', $this->accionUsuario]);

        $query->orderBy("E.nombre, A.item_name, U.name, U.fechaCreacion");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param Empresa $empresa
     * @param type $params
     * @return ActiveDataProvider
     */
    public function buscarUsuarios($empresa, $params) {
        $query = Usuario::find()
                ->select("U.*, E.*, A.*")
                ->from("Usuario U")
                ->leftJoin("Empresa E", "U.idEmpresa = E.idEmpresa")
                ->leftJoin("auth_assignment A", "A.user_id = U.id")
                ->where("A.item_name IN ('" . Constants::ROL_COORDINADOR . "', '" . Constants::ROL_PROYECTO . "')")
                ->andWhere("E.idEmpresa = $empresa->idEmpresa");

        $query->andFilterWhere([
            'U.id' => $this->id,
            'E.idEmpresa' => $this->idEmpresa,
            'U.idUsuarioActualizo' => $this->idUsuarioActualizo,
            'U.fechaCreacion' => $this->fechaCreacion,
            'U.fechaModificacion' => $this->fechaModificacion,
        ]);

        $query->andFilterWhere(['like', 'U.name', $this->name])
                ->andFilterWhere(['like', 'U.password', $this->password])
                ->andFilterWhere(['like', 'U.email', $this->email])
                ->andFilterWhere(['like', 'U.accessToken', $this->accessToken])
                ->andFilterWhere(['like', 'U.authKey', $this->authKey])
                ->andFilterWhere(['like', 'U.accionUsuario', $this->accionUsuario]);

        $query->orderBy("A.item_name, U.name, U.fechaCreacion");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     * @param Usuario $usuario
     * @param type $params
     * @return ActiveDataProvider
     */
    public function buscarProyectosCoordinador($usuario, $params) {
        $empresa = $usuario->empresa;

        $query = Usuario::find()
                ->select("U.*, E.*, A.*")
                ->from("Usuario U")
                ->leftJoin("Empresa E", "U.idEmpresa = E.idEmpresa")
                ->leftJoin("auth_assignment A", "A.user_id = U.id")
                ->where("A.item_name IN ('" . Constants::ROL_PROYECTO . "')")
                ->andWhere("E.idEmpresa = $empresa->idEmpresa")
                ->andWhere("U.idUsuarioRegistro = $usuario->id");

        $query->orderBy("U.name, U.fechaCreacion");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 200,
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }

}
