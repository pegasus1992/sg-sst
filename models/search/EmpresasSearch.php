<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\logic\Empresa;

/**
 * EmpresasSearch represents the model behind the search form of `app\models\logic\Empresa`.
 */
class EmpresasSearch extends Empresa {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['idEmpresa', 'idUsuarioActualizo'], 'integer'],
            [['nombre', 'nit', 'rutaLogo', 'accionUsuario', 'fechaCreacion', 'fechaModificacion'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Empresa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idEmpresa' => $this->idEmpresa,
            'idUsuarioActualizo' => $this->idUsuarioActualizo,
            'fechaCreacion' => $this->fechaCreacion,
            'fechaModificacion' => $this->fechaModificacion,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
                ->andFilterWhere(['like', 'nit', $this->nit])
                ->andFilterWhere(['like', 'rutaLogo', $this->rutaLogo])
                ->andFilterWhere(['like', 'accionUsuario', $this->accionUsuario]);

        return $dataProvider;
    }

}
