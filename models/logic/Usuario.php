<?php

namespace app\models\logic;

use Yii;
use app\models\rbac\AuthAssignment;
use app\models\rbac\AuthItem;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $accessToken
 * @property string $authKey
 * @property int $idEmpresa
 * @property int $idUsuarioRegistro
 * @property int $idUsuarioActualizo
 * @property string $accionUsuario
 * @property string $fechaCreacion
 * @property string $fechaModificacion
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItem[] $itemNames
 * @property Documento[] $documentos
 * @property Empresa $empresa
 */
class Usuario extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name', 'email', 'password', 'idEmpresa', 'idUsuarioRegistro', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['idEmpresa', 'idUsuarioActualizo'], 'integer'],
            [['fechaCreacion', 'fechaModificacion'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['email'], 'string', 'max' => 100],
            [['password'], 'string', 'max' => 40],
            [['accessToken', 'authKey'], 'string', 'max' => 250],
            [['accionUsuario'], 'string', 'max' => 1],
            [['email'], 'unique'],
            [['idEmpresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idEmpresa' => 'idEmpresa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'email' => 'Correo',
            'password' => 'Contraseña',
            'accessToken' => 'Access Token',
            'authKey' => 'Auth Key',
            'idEmpresa' => 'Empresa',
            'idUsuarioRegistro' => 'Asignar a',
            'idUsuarioActualizo' => 'Id Usuario Actualizo',
            'accionUsuario' => 'Accion Usuario',
            'fechaCreacion' => 'Fecha Creacion',
            'fechaModificacion' => 'Fecha Modificacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments() {
        return $this->hasMany(AuthAssignment::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemNames() {
        return $this->hasMany(AuthItem::className(), ['name' => 'item_name'])->viaTable('auth_assignment', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos() {
        return $this->hasMany(Documento::className(), ['idUsuario' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa() {
        return $this->hasOne(Empresa::className(), ['idEmpresa' => 'idEmpresa']);
    }

}
