<?php

namespace app\models\logic;

use Yii;

/**
 * This is the model class for table "tarifa".
 *
 * @property int $idTarifa
 * @property string $codigo
 * @property string $nombre
 * @property string $descripcion
 * @property string $rutaImagen
 * @property double $valor
 * @property string $estado estado: A -> Activo I -> Inactivo
 * @property int $diasAplica
 * @property int $mesesAplica
 * @property int $idUsuarioRegistro
 * @property int $idUsuarioActualizo
 * @property string $accionUsuario
 * @property string $fechaCreacion
 * @property string $fechaModificacion
 *
 * @property Pago[] $pagos
 */
class Tarifa extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'tarifa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['codigo', 'nombre', 'descripcion', 'valor', 'idUsuarioRegistro', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['valor'], 'number'],
            [['diasAplica', 'mesesAplica', 'idUsuarioRegistro', 'idUsuarioActualizo'], 'integer'],
            [['fechaCreacion', 'fechaModificacion'], 'safe'],
            [['codigo'], 'string', 'max' => 5],
            [['nombre'], 'string', 'max' => 45],
            [['descripcion', 'rutaImagen'], 'string', 'max' => 500],
            [['estado', 'accionUsuario'], 'string', 'max' => 1],
            [['valor'], 'compare', 'compareValue' => 0, 'operator' => '>='],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idTarifa' => 'Id Tarifa',
            'codigo' => 'Código',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'rutaImagen' => 'Imagen',
            'valor' => 'Valor',
            'estado' => 'Estado',
            'diasAplica' => 'Dias que Aplica',
            'mesesAplica' => 'Meses que Aplica',
            'idUsuarioRegistro' => 'Id Usuario Registro',
            'idUsuarioActualizo' => 'Id Usuario Actualizo',
            'accionUsuario' => 'Accion Usuario',
            'fechaCreacion' => 'Fecha Creacion',
            'fechaModificacion' => 'Fecha Modificacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos() {
        return $this->hasMany(Pago::className(), ['idTarifa' => 'idTarifa']);
    }

}
