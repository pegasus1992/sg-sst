<?php

namespace app\models\logic;

use Yii;

/**
 * This is the model class for table "categoria".
 *
 * @property int $idCategoria
 * @property string $nombre
 * @property string $formato
 * @property string $fechaDiligenciamiento
 * @property int $idCategoriaPadre
 * @property int $nivel
 * @property string $tipo tipo: M -> Mayor A -> Auxiliar C -> Combinado (Auxiliar que tiene auxiliares)
 * @property string $soloArchivos
 *
 * @property Categoria $categoriaPadre
 * @property Categoria[] $categorias
 * @property Documento[] $documentos
 * @property Permiso[] $permisos
 * @property AuthItem[] $rols
 */
class Categoria extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'categoria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre', 'tipo'], 'required'],
            [['idCategoriaPadre', 'nivel'], 'integer'],
            [['nombre'], 'string', 'max' => 60],
            [['formato'], 'string', 'max' => 75],
            [['fechaDiligenciamiento'], 'string', 'max' => 150],
            [['tipo', 'soloArchivos'], 'string', 'max' => 1],
            [['idCategoriaPadre'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['idCategoriaPadre' => 'idCategoria']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idCategoria' => 'Id Categoria',
            'nombre' => 'Nombre',
            'formato' => 'Formato',
            'fechaDiligenciamiento' => 'Fecha Diligenciamiento',
            'idCategoriaPadre' => 'Id Categoria Padre',
            'nivel' => 'Nivel',
            'tipo' => 'Tipo',
            'soloArchivos' => 'Solo Archivos',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoriaPadre() {
        return $this->hasOne(Categoria::className(), ['idCategoria' => 'idCategoriaPadre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategorias() {
        return $this->hasMany(Categoria::className(), ['idCategoriaPadre' => 'idCategoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos() {
        return $this->hasMany(Documento::className(), ['idCategoria' => 'idCategoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPermisos() {
        return $this->hasMany(Permiso::className(), ['idCategoria' => 'idCategoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRols() {
        return $this->hasMany(AuthItem::className(), ['name' => 'rol'])->viaTable('permiso', ['idCategoria' => 'idCategoria']);
    }

}
