<?php

namespace app\models\logic;

use Yii;

/**
 * This is the model class for table "pago".
 *
 * @property int $idPago
 * @property int $idEmpresa
 * @property string $fechaInicial
 * @property string $fechaFinal
 * @property int $idTarifa
 * @property double $porcentajeIva
 * @property double $valorDescuento
 * @property string $estado estado: A -> Activo N -> Anulado
 * @property int $idUsuarioRegistro
 * @property int $idUsuarioActualizo
 * @property string $accionUsuario
 * @property string $fechaCreacion
 * @property string $fechaModificacion
 *
 * @property Empresa $empresa
 * @property Tarifa $tarifa
 */
class Pago extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'pago';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idEmpresa', 'idTarifa', 'fechaInicial', 'fechaFinal', 'idUsuarioRegistro', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['idEmpresa', 'idTarifa', 'idUsuarioRegistro', 'idUsuarioActualizo'], 'integer'],
            [['fechaInicial', 'fechaFinal', 'fechaCreacion', 'fechaModificacion'], 'safe'],
            [['porcentajeIva', 'valorDescuento'], 'number'],
            [['estado', 'accionUsuario'], 'string', 'max' => 1],
            [['idEmpresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idEmpresa' => 'idEmpresa']],
            [['idTarifa'], 'exist', 'skipOnError' => true, 'targetClass' => Tarifa::className(), 'targetAttribute' => ['idTarifa' => 'idTarifa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idPago' => 'Pago #',
            'idEmpresa' => 'Empresa',
            'fechaInicial' => 'Fecha Inicial',
            'fechaFinal' => 'Fecha Final',
            'idTarifa' => 'Tarifa Aplicada',
            'porcentajeIva' => '% IVA',
            'valorDescuento' => 'Descuento',
            'estado' => 'Estado',
            'idUsuarioRegistro' => 'Id Usuario Registro',
            'idUsuarioActualizo' => 'Id Usuario Actualizo',
            'accionUsuario' => 'Accion Usuario',
            'fechaCreacion' => 'Fecha Creacion',
            'fechaModificacion' => 'Fecha Modificacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresa() {
        return $this->hasOne(Empresa::className(), ['idEmpresa' => 'idEmpresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifa() {
        return $this->hasOne(Tarifa::className(), ['idTarifa' => 'idTarifa']);
    }

    /**
     * Calcula el valor total de una factura.
     * @return double
     */
    public function getTotal() {
        $subtotal = $this->getTotalSinIVA();
        $porcIva = $this->porcentajeIva;

        $iva = $subtotal * $porcIva / 100;
        $total = $subtotal - $iva;
        return $total;
    }

    /**
     * Calcula el total de una factura, antes del IVA.
     * @return double
     */
    public function getTotalSinIVA() {
        $subtotal = $this->tarifa->valor;
        $descuento = $this->valorDescuento;

        $val1 = $subtotal - $descuento;
        $val2 = $val1 >= 0.0 ? $val1 : 0.0;
        return $val2;
    }

}
