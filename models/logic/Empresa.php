<?php

namespace app\models\logic;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $idEmpresa
 * @property string $nombre
 * @property string $nit
 * @property string $rutaLogo
 * @property int $idUsuarioActualizo
 * @property string $accionUsuario
 * @property string $fechaCreacion
 * @property string $fechaModificacion
 *
 * @property Pago[] $pagos
 * @property Usuario[] $usuarios
 */
class Empresa extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['nombre', 'nit', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['idUsuarioActualizo'], 'integer'],
            [['fechaCreacion', 'fechaModificacion'], 'safe'],
            [['nombre'], 'string', 'max' => 50],
            [['nit'], 'string', 'max' => 11],
            [['rutaLogo'], 'string', 'max' => 500],
            [['accionUsuario'], 'string', 'max' => 1],
            [['nit'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idEmpresa' => 'Id Empresa',
            'nombre' => 'Nombre',
            'nit' => 'Nit',
            'rutaLogo' => 'Ruta Logo',
            'idUsuarioActualizo' => 'Id Usuario Actualizo',
            'accionUsuario' => 'Accion Usuario',
            'fechaCreacion' => 'Fecha Creacion',
            'fechaModificacion' => 'Fecha Modificacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPagos() {
        return $this->hasMany(Pago::className(), ['idEmpresa' => 'idEmpresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarios() {
        return $this->hasMany(Usuario::className(), ['idEmpresa' => 'idEmpresa']);
    }

}
