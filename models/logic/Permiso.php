<?php

namespace app\models\logic;

use Yii;

/**
 * This is the model class for table "permiso".
 *
 * @property string $rol
 * @property int $idCategoria
 * @property int $crearCarpeta
 * @property int $borrarCarpeta
 * @property int $subirPlantilla
 * @property int $borrarPlantilla
 * @property int $subirArchivo
 * @property int $borrarArchivo
 *
 * @property Categoria $categoria
 * @property AuthItem $rol0
 */
class Permiso extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'permiso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['rol', 'idCategoria'], 'required'],
            [['idCategoria', 'crearCarpeta', 'borrarCarpeta', 'subirPlantilla', 'borrarPlantilla', 'subirArchivo', 'borrarArchivo'], 'integer'],
            [['rol'], 'string', 'max' => 64],
            [['rol', 'idCategoria'], 'unique', 'targetAttribute' => ['rol', 'idCategoria']],
            [['idCategoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['idCategoria' => 'idCategoria']],
            [['rol'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['rol' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'rol' => 'Rol',
            'idCategoria' => 'Id Categoria',
            'crearCarpeta' => 'Crear Carpeta',
            'borrarCarpeta' => 'Borrar Carpeta',
            'subirPlantilla' => 'Subir Plantilla',
            'borrarPlantilla' => 'Borrar Plantilla',
            'subirArchivo' => 'Subir Archivo',
            'borrarArchivo' => 'Borrar Archivo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria() {
        return $this->hasOne(Categoria::className(), ['idCategoria' => 'idCategoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRol0() {
        return $this->hasOne(AuthItem::className(), ['name' => 'rol']);
    }

}
