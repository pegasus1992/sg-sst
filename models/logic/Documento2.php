<?php

namespace app\models\logic;

use Yii;

class Documento2 extends Documento {

    const PERMISSIONS_PRIVATE = 10;
    const PERMISSIONS_PUBLIC = 20;

    public $archivo;

    public function rules() {
        return [
            [['idUsuario', 'idCategoria', 'rutaArchivo', 'archivo', 'esFichero', 'esPlantilla', 'idUsuarioRegistro', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['idUsuario', 'idCategoria', 'idFicheroPadre', 'idUsuarioRegistro', 'idUsuarioActualizo'], 'integer'],
            [['fechaRegistro', 'fechaModificacion'], 'safe'],
            [['rutaArchivo'], 'string', 'max' => 254],
            [['archivo'], 'safe'],
            [['archivo'], 'file', 'extensions' => 'pdf, doc, docx, xls, xlsx, ppt, pptx'],
            [['archivo'], 'file', 'maxSize' => 1024 * 1024 * 50], // 50 MB Maximo
            [['esFichero', 'esPlantilla', 'eliminado', 'accionUsuario'], 'string', 'max' => 1],
            [['idCategoria', 'rutaArchivo', 'esFichero', 'idFicheroPadre', 'esPlantilla', 'eliminado'], 'unique', 'targetAttribute' => ['idCategoria', 'rutaArchivo', 'esFichero', 'idFicheroPadre', 'esPlantilla', 'eliminado']],
            [['idFicheroPadre'], 'exist', 'skipOnError' => true, 'targetClass' => Documento::className(), 'targetAttribute' => ['idFicheroPadre' => 'idDocumento']],
            [['idCategoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['idCategoria' => 'idCategoria']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
        ];
    }

    public function attributeLabels() {
        return [
            'idDocumento' => 'Documento',
            'idUsuario' => 'Usuario',
            'idCategoria' => 'Categoría',
            'rutaArchivo' => 'Archivo',
            'archivo' => 'Archivo',
            'esFichero' => 'Es Fichero',
            'idFicheroPadre' => 'Fichero Padre',
            'esPlantilla' => 'Es Plantilla',
            'eliminado' => 'Eliminado',
            'idUsuarioRegistro' => 'Asignado a',
            'idUsuarioActualizo' => 'Usuario Actualizó',
            'accionUsuario' => 'Accion Usuario',
            'fechaRegistro' => 'Fecha Registro',
            'fechaModificacion' => 'Fecha Modificación',
        ];
    }

}
