<?php

namespace app\models\logic;

use Yii;

class Perfil extends Usuario {

    public $oldpassword;
    public $newpassword;
    public $repassword;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['password', 'oldpassword', 'newpassword', 'repassword', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['idEmpresa', 'idUsuarioActualizo'], 'integer'],
            [['fechaCreacion', 'fechaModificacion'], 'safe'],
            [['password'], 'string', 'max' => 40],
            [['oldpassword'], 'string', 'max' => 40],
            [['newpassword'], 'string', 'max' => 40],
            [['repassword'], 'string', 'max' => 40],
//            ['repassword', 'compare', 'compareAttribute' => 'newpassword'],
            [['accionUsuario'], 'string', 'max' => 1],
            [['email'], 'unique'],
            [['idEmpresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['idEmpresa' => 'idEmpresa']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'password' => 'Contraseña',
            'oldpassword' => 'Contraseña Actual',
            'newpassword' => 'Nueva Contraseña',
            'repassword' => 'Repita la Contraseña',
        ];
    }

}
