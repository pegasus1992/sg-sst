<?php

namespace app\models\logic;

use Yii;

/**
 * This is the model class for table "documento".
 *
 * @property int $idDocumento
 * @property int $idUsuario
 * @property int $idCategoria
 * @property string $rutaArchivo
 * @property string $esFichero
 * @property int $idFicheroPadre
 * @property string $esPlantilla
 * @property string $eliminado
 * @property int $idUsuarioRegistro
 * @property int $idUsuarioActualizo
 * @property string $accionUsuario
 * @property string $fechaRegistro
 * @property string $fechaModificacion
 *
 * @property Documento $ficheroPadre
 * @property Documento[] $documentos
 * @property Categoria $categoria
 * @property Usuario $usuario
 */
class Documento extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['idUsuario', 'idCategoria', 'rutaArchivo', 'esFichero', 'esPlantilla', 'idUsuarioRegistro', 'idUsuarioActualizo', 'accionUsuario'], 'required'],
            [['idUsuario', 'idCategoria', 'idFicheroPadre', 'idUsuarioRegistro', 'idUsuarioActualizo'], 'integer'],
            [['fechaRegistro', 'fechaModificacion'], 'safe'],
            [['rutaArchivo'], 'string', 'max' => 254],
            [['esFichero', 'esPlantilla', 'eliminado', 'accionUsuario'], 'string', 'max' => 1],
            [['idCategoria', 'rutaArchivo', 'esFichero', 'idFicheroPadre', 'esPlantilla', 'eliminado'], 'unique', 'targetAttribute' => ['idCategoria', 'rutaArchivo', 'esFichero', 'idFicheroPadre', 'esPlantilla', 'eliminado']],
            [['idFicheroPadre'], 'exist', 'skipOnError' => true, 'targetClass' => Documento::className(), 'targetAttribute' => ['idFicheroPadre' => 'idDocumento']],
            [['idCategoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categoria::className(), 'targetAttribute' => ['idCategoria' => 'idCategoria']],
            [['idUsuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['idUsuario' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'idDocumento' => 'Documento',
            'idUsuario' => 'Usuario',
            'idCategoria' => 'Categoría',
            'rutaArchivo' => 'Archivo',
            'esFichero' => 'Es Fichero',
            'idFicheroPadre' => 'Fichero Padre',
            'esPlantilla' => 'Es Plantilla',
            'eliminado' => 'Eliminado',
            'idUsuarioRegistro' => 'Asignado a',
            'idUsuarioActualizo' => 'Usuario Actualizó',
            'accionUsuario' => 'Accion Usuario',
            'fechaRegistro' => 'Fecha Registro',
            'fechaModificacion' => 'Fecha Modificación',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFicheroPadre() {
        return $this->hasOne(Documento::className(), ['idDocumento' => 'idFicheroPadre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentos() {
        return $this->hasMany(Documento::className(), ['idFicheroPadre' => 'idDocumento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategoria() {
        return $this->hasOne(Categoria::className(), ['idCategoria' => 'idCategoria']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario() {
        return $this->hasOne(Usuario::className(), ['id' => 'idUsuario']);
    }

}
