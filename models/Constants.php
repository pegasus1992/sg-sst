<?php

namespace app\models;

/**
 * Description of Constants
 *
 * @author Avuunita
 */
class Constants {

    //
    const accionUsuarioInsertar = "I";
    const accionUsuarioModificar = "M";
    const accionUsuarioBorrar = "B";
    const accionUsuarioAnular = "A";
    //
    const tipoMayor = "M";
    const tipoAuxiliar = "A";
    const tipoCombinado = "C";
    //
    const varSiBD = "S";
    const varNoBD = "N";
    //
    const varSi = "Sí";
    const varNo = "No";
    //
    const estadoActivoBD = "A";
    const estadoInactivoBD = "I";
    const estadoAnuladoBD = "N";
    //
    const estadoActivo = "Activo";
    const estadoInactivo = "Inactivo";
    const estadoAnulado = "Anulado";
    //
    const directorioPlantilla = "Plantilla/";
    //
    const ROL_SUPERVISOR = "supervisor";
    const ROL_DIRECTOR = "director";
    const ROL_COORDINADOR = "coordinador";
    const ROL_PROYECTO = "proyecto";
    //
    const NUM_SUPERVISOR = "1";
    const NUM_DIRECTOR = "2";
    const NUM_COORDINADOR = "3";
    const NUM_PROYECTO = "4";
    //
    const ACCION_CREAR_CARPETA = 1;
    const ACCION_BORRAR_CARPETA = 2;
    const ACCION_SUBIR_ARCHIVO = 3;
    const ACCION_BORRAR_ARCHIVO = 4;
    const ACCION_SUBIR_PLANTILLA = 5;
    const ACCION_BORRAR_PLANTILLA = 6;
    //
    const VALOR_DESCUENTO_DEFAULT = 0;
    const PORCENTAJE_IVA_DEFAULT = 0;

}
