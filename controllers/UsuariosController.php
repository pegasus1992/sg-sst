<?php

namespace app\controllers;

use Yii;
use app\models\Constants;
use app\models\Utils;
use app\models\logic\Empresa;
use app\models\logic\Usuario;
use app\models\search\UsuariosSearch;
use app\models\rbac\AuthAssignment;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Description of UsuariosController
 *
 * @author Avuunita
 */
class UsuariosController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['directores'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['usuarios'],
                        'roles' => [Constants::NUM_DIRECTOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['director'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['usuario'],
                        'roles' => [Constants::NUM_DIRECTOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-director'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-coordinador', 'create-proyecto'],
                        'roles' => [Constants::NUM_DIRECTOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-director', 'restore-director'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-coordinador', 'update-proyecto', 'restore-coordinador', 'restore-proyecto'],
                        'roles' => [Constants::NUM_DIRECTOR],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['delete'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'restore-director' => ['POST'],
                    'restore-usuario' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lista todos los usuarios directores registrados.
     * @return mixed
     */
    public function actionDirectores() {
        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->buscarDirectores(Yii::$app->request->queryParams);

        return $this->render('directores', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lista todos los usuarios coordinadores y proyectos registrados.
     * @return mixed
     */
    public function actionUsuarios() {
        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $empresa = $usuario->empresa;

        $searchModel = new UsuariosSearch();
        $dataProvider = $searchModel->buscarUsuarios($empresa, Yii::$app->request->queryParams);

        return $this->render('usuarios', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Muestra la informacion de un director.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDirector($id) {
        return $this->render('director', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Muestra la informacion de un usuario coordinador o proyecto.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUsuario($id) {
        $this->comprobarAcceso($id);

        return $this->render('usuario', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Registra un nuevo director.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateDirector() {
        $model = new Usuario();
        $empresas = $this->getEmpresas();
        $model->idUsuarioRegistro = Yii::$app->user->id;

        $this->crearUsuario($model, Constants::ROL_DIRECTOR);

        return $this->render('create-director', [
                    'model' => $model,
                    'empresas' => $empresas,
        ]);
    }

    /**
     * Registra un nuevo coordinador.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateCoordinador() {
        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $empresa = $usuario->empresa;

        $model = new Usuario();
        $model->idEmpresa = $empresa->idEmpresa;
        $model->idUsuarioRegistro = Yii::$app->user->id;

        $this->crearUsuario($model, Constants::ROL_COORDINADOR);

        return $this->render('create-coordinador', [
                    'model' => $model,
        ]);
    }

    /**
     * Registra un nuevo proyecto.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateProyecto() {
        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $empresa = $usuario->empresa;

        $model = new Usuario();
        $model->idEmpresa = $empresa->idEmpresa;
        $usuarios = $this->getCoordinadores($model->idEmpresa);

        $this->crearUsuario($model, Constants::ROL_PROYECTO);

        return $this->render('create-proyecto', [
                    'model' => $model,
                    'usuarios' => $usuarios,
        ]);
    }

    /**
     * Modifica un director existente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateDirector($id) {
        $model = $this->findModel($id);
        $oldEmail = $model->email;
        $empresas = $this->getEmpresas();

        $this->modificarUsuario($model, Constants::ROL_DIRECTOR, $oldEmail);

        return $this->render('update-director', [
                    'model' => $model,
                    'empresas' => $empresas,
        ]);
    }

    /**
     * Modifica un coordinador existente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateCoordinador($id) {
        $this->comprobarAcceso($id);

        $model = $this->findModel($id);
        $oldEmail = $model->email;

        $this->modificarUsuario($model, Constants::ROL_COORDINADOR, $oldEmail);

        return $this->render('update-coordinador', [
                    'model' => $model,
        ]);
    }

    /**
     * Modifica un proyecto existente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdateProyecto($id) {
        $this->comprobarAcceso($id);

        $model = $this->findModel($id);
        $oldEmail = $model->email;
        $usuarios = $this->getCoordinadores($model->idEmpresa);

        $this->modificarUsuario($model, Constants::ROL_PROYECTO, $oldEmail);

        return $this->render('update-proyecto', [
                    'model' => $model,
                    'usuarios' => $usuarios,
        ]);
    }

    /**
     * Reestablece la contrasenia de un director existente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRestoreDirector($id) {
        $model = $this->findModel($id);

        $this->reestablecerContrasenia($model, Constants::ROL_DIRECTOR);
    }

    /**
     * Reestablece la contrasenia de un coordinador existente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRestoreCoordinador($id) {
        $this->comprobarAcceso($id);

        $model = $this->findModel($id);

        $this->reestablecerContrasenia($model, Constants::ROL_COORDINADOR);
    }

    /**
     * Reestablece la contrasenia de un proyecto existente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRestoreProyecto($id) {
        $this->comprobarAcceso($id);

        $model = $this->findModel($id);

        $this->reestablecerContrasenia($model, Constants::ROL_PROYECTO);
    }

    /**
     * 
     * @param Usuario $model
     * @param string $rol
     * @return type
     */
    private function crearUsuario($model, $rol) {
        if ($model->load(Yii::$app->request->post())) {
            $password = Utils::rand_pass();
            $model->password = sha1($password);

            $model->idUsuarioActualizo = Yii::$app->user->id;
            $model->accionUsuario = Constants::accionUsuarioInsertar;
            $model->fechaCreacion = date('Y-m-d H:i:s');
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->save()) {
                $auth = new AuthAssignment();
                $auth->item_name = $rol;
                $auth->user_id = $model->id;

                if ($auth->save()) {
                    $saved = true;
                }
            }

            if ($saved) {
                $transaction->commit();

                try {
                    $subject = "Creación exitosa de la cuenta";
                    $content = "<p>Su cuenta se ha creado exitosamente.</p>";
                    $content .= "<p>Datos para el login:</p>";
                    $content .= "<p>Correo: $model->email</p>";
                    $content .= "<p>Contraseña: $password</p>";
                    $content .= "<p>Recuerde que debe cambiar su contraseña por seguridad.</p>";

                    Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                            ->setTo($model->email)
                            ->setFrom([$model->email => $model->name])
                            ->setSubject($subject)
                            ->setTextBody($content)
                            ->send();

                    Yii::$app->session->setFlash('success', "La cuenta se ha creado exitosamente. Se ha enviado un correo con sus datos ingresados. Favor cambiar la contraseña de inmediato.");
                } catch (Exception $ex) {
                    //
                } catch (yii\base\ErrorException $ex) {
                    //
                }

                if ($rol == Constants::ROL_DIRECTOR) {
                    return $this->redirect(['director', 'id' => $model->id]);
                } else if ($rol == Constants::ROL_COORDINADOR || $rol == Constants::ROL_PROYECTO) {
                    return $this->redirect(['usuario', 'id' => $model->id]);
                }
            } else {
                $transaction->rollback();
            }
        }
    }

    /**
     * 
     * @param Usuario $model
     * @param string $rol
     * @param string $oldEmail
     * @return type
     */
    private function modificarUsuario($model, $rol, $oldEmail) {
        if ($model->load(Yii::$app->request->post())) {
            $model->idUsuarioActualizo = Yii::$app->user->id;
            $model->accionUsuario = Constants::accionUsuarioModificar;
            $model->fechaModificacion = date('Y-m-d H:i:s');

            if ($model->update()) {
                if ($oldEmail !== $model->email) {
                    try {
                        $subject = "Modificación exitosa de la cuenta";
                        $content = "<p>Se ha cambiado recientemente su cuenta.</p>";
                        $content .= "<p>Nuevos datos para el login:</p>";
                        $content .= "<p>Usuario: $model->email</p>";
                        $content .= "<p>Contraseña: <b>La misma que tiene registrada.</b></p>";
                        $content .= "<p>Recuerde que debe cambiar su contraseña por seguridad.</p>";

                        Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                                ->setTo($model->email)
                                ->setFrom([$model->email => $model->name])
                                ->setSubject($subject)
                                ->setTextBody($content)
                                ->send();

                        Yii::$app->session->setFlash('success', "La cuenta se ha modificado exitosamente. Se ha enviado un correo con sus datos ingresados. Favor cambiar la contraseña de inmediato.");
                    } catch (Exception $ex) {
                        //
                    } catch (\yii\base\ErrorException $ex) {
                        //
                    }
                }

                if ($rol == Constants::ROL_DIRECTOR) {
                    return $this->redirect(['director', 'id' => $model->id]);
                } else if ($rol == Constants::ROL_COORDINADOR || $rol == Constants::ROL_PROYECTO) {
                    return $this->redirect(['usuario', 'id' => $model->id]);
                }
            }
        }
    }

    /**
     * 
     * @param Usuario $model
     * @param string $rol
     */
    private function reestablecerContrasenia($model, $rol) {
        $password = Utils::rand_pass();
        $model->password = sha1($password);

        $model->idUsuarioActualizo = Yii::$app->user->id;
        $model->accionUsuario = Constants::accionUsuarioModificar;
        $model->fechaModificacion = date('Y-m-d H:i:s');

        if ($model->update()) {
            try {
                $subject = "Contraseña reestablecida correctamente";
                $content = "<p>Se ha reestablecido recientemente su contraseña.</p>";
                $content .= "<p>Nuevos datos para el login:</p>";
                $content .= "<p>Usuario: <b>La misma que tiene registrada.</b></p>";
                $content .= "<p>Contraseña: $password</p>";
                $content .= "<p>Recuerde que debe cambiar su contraseña por seguridad.</p>";

                Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
                        ->setTo($model->email)
                        ->setFrom([$model->email => $model->name])
                        ->setSubject($subject)
                        ->setTextBody($content)
                        ->send();

                Yii::$app->session->setFlash('success', "La cuenta se ha modificado exitosamente. Se ha enviado un correo con sus datos ingresados. Favor cambiar la contraseña de inmediato.");
            } catch (Exception $ex) {
                //
            } catch (\yii\base\ErrorException $ex) {
                //
            }
        }

        if ($rol == Constants::ROL_DIRECTOR) {
            return $this->redirect(['directores']);
        } else if ($rol == Constants::ROL_COORDINADOR || $rol == Constants::ROL_PROYECTO) {
            return $this->redirect(['usuarios']);
        }
    }

    private function getEmpresas() {
        return ArrayHelper::map(Empresa::find()->all(), 'idEmpresa', 'nombre');
    }

    private function getCoordinadores($idEmpresa) {
        $query = Usuario::find()
                ->select("U.*, E.*, A.*")
                ->from("Usuario U")
                ->leftJoin("Empresa E", "U.idEmpresa = E.idEmpresa")
                ->leftJoin("auth_assignment A", "A.user_id = U.id")
                ->where("E.idEmpresa = $idEmpresa")
                ->andWhere("A.item_name IN ('" . Constants::ROL_COORDINADOR . "')")
                ->all();
        $r = ArrayHelper::map($query, 'id', 'name');
        return $r;
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private static function comprobarAcceso($idUsuario) {
        if (is_null($idUsuario) || (!is_null($idUsuario) && strlen(trim($idUsuario)) == 0)) {
            throw new NotFoundHttpException('Usuario no ingresado.');
        }

        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $empresa = $usuario->empresa;

        $usuarioIngresado = Usuario::findOne($idUsuario);
        if (!isset($usuarioIngresado)) {
            throw new NotFoundHttpException('Usuario no encontrado.');
        }
        $empresaIngresada = $usuarioIngresado->empresa;

        if ($empresaIngresada->idEmpresa != $empresa->idEmpresa) {
            throw new ForbiddenHttpException('No tiene acceso a esa información.');
        }
    }

}
