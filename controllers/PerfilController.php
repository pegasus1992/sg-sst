<?php

namespace app\controllers;

use Yii;
use app\models\Constants;
use app\models\logic\Usuario;
use app\models\logic\Perfil;
use app\models\logic\Empresa;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * Description of PerfilController
 *
 * @author Avuunita
 */
class PerfilController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'update', 'change'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['empresa'],
                        'roles' => [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex() {
        $id = Yii::$app->user->id;
        $model = $this->findModel($id);

        return $this->render('index', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate() {
        $id = Yii::$app->user->id;
        $model = $this->findModel($id);
        $rol = $model->authAssignments[0]->item_name;
        $email = $model->email;

        if ($model->load(Yii::$app->request->post())) {
            if (($rol == Constants::ROL_COORDINADOR || $rol == Constants::ROL_PROYECTO) && $model->email != $email) {
                $model->email = $email;
            }

            $model->idUsuarioActualizo = Yii::$app->user->id;
            $model->accionUsuario = Constants::accionUsuarioModificar;
            $model->fechaModificacion = date('Y-m-d H:i:s');

            if ($model->update()) {
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionChange() {
        $id = Yii::$app->user->id;
        $model = $this->findModel2($id);

        if ($model->load(Yii::$app->request->post())) {
            $password = $model->password;

            $oldpassword = $model->oldpassword;
            $newpassword = $model->newpassword;
            $repassword = $model->repassword;

            $old = sha1($oldpassword);
            $new = sha1($newpassword);

            $valido = false;
            if ($old == $password) {
                if ($old != $new) {
                    if ($newpassword == $repassword) {
                        $valido = true;
                    } else {
                        $model->addError('repassword', "Las contraseñas deben coincidir.");
                        $model->repassword = "";
                    }
                } else {
                    $model->addError('newpassword', "La contraseña nueva debe ser diferente a la actual.");
                    $model->newpassword = "";
                    $model->repassword = "";
                }
            } else {
                $model->addError('oldpassword', "La contraseña actual no coincide.");
                $model->oldpassword = "";
                $model->newpassword = "";
                $model->repassword = "";
            }

            if ($valido) {
                $model->password = sha1($model->newpassword);
                $model->idUsuarioActualizo = Yii::$app->user->id;
                $model->accionUsuario = Constants::accionUsuarioModificar;
                $model->fechaModificacion = date('Y-m-d H:i:s');

                if ($model->update()) {
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('change', [
                    'model' => $model,
        ]);
    }

    public function actionEmpresa() {
        $id = Yii::$app->user->id;
        $model = $this->findModel($id);
        $idEmpresa = $model->empresa->idEmpresa;
        $empresa = Empresa::findOne($idEmpresa);
        $logoActual = $empresa->rutaLogo;

        if ($empresa->load(Yii::$app->request->post())) {
            $logo = UploadedFile::getInstance($empresa, 'rutaLogo');
            if (isset($logo)) {
                $empresa->rutaLogo = $logo->name;
            } else {
                $empresa->rutaLogo = $logoActual;
            }

            $empresa->idUsuarioActualizo = Yii::$app->user->id;
            $empresa->accionUsuario = Constants::accionUsuarioModificar;
            $empresa->fechaModificacion = date('Y-m-d H:i:s');

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($empresa->update()) {
                try {
                    if (isset($logo)) {
                        $path = Yii::$app->basePath . '/web/Logos/';
                        $filename = $logo->baseName . '.' . $logo->extension;
                        $saved = $logo->saveAs($path . $filename);
                    } else {
                        $saved = true;
                    }
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect(['index']);
            } else {
                $transaction->rollback();

                $empresa->rutaLogo = $logoActual;
            }
        }

        return $this->render('empresa', [
                    'model' => $model,
                    'empresa' => $empresa,
        ]);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Perfil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel2($id) {
        if (($model = Perfil::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
