<?php

namespace app\controllers;

use Yii;
use app\models\Constants;
use app\models\logic\Empresa;
use app\models\search\EmpresasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EmpresasController implements the CRUD actions for Empresa model.
 */
class EmpresasController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['view'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => false,
                        'actions' => ['delete'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EmpresasSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Empresa();
        $logoActual = $model->rutaLogo;

        if ($model->load(Yii::$app->request->post())) {
            $logo = UploadedFile::getInstance($model, 'rutaLogo');
            if (isset($logo)) {
                $model->rutaLogo = $logo->name;
            } else {
                $model->rutaLogo = $logoActual;
            }

            $model->idUsuarioActualizo = Yii::$app->user->id;
            $model->accionUsuario = Constants::accionUsuarioInsertar;
            $model->fechaCreacion = date('Y-m-d H:i:s');
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->save()) {
                try {
                    if (isset($logo)) {
                        $path = Yii::$app->basePath . '/web/Logos/';
                        $filename = $logo->baseName . '.' . $logo->extension;
                        $saved = $logo->saveAs($path . $filename);
                    } else {
                        $saved = true;
                    }
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->idEmpresa]);
            } else {
                $transaction->rollback();

                $model->rutaLogo = $logoActual;
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $logoActual = $model->rutaLogo;

        if ($model->load(Yii::$app->request->post())) {
            $logo = UploadedFile::getInstance($model, 'rutaLogo');
            if (isset($logo)) {
                $model->rutaLogo = $logo->name;
            } else {
                $model->rutaLogo = $logoActual;
            }

            $model->idUsuarioActualizo = Yii::$app->user->id;
            $model->accionUsuario = Constants::accionUsuarioModificar;
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->update()) {
                try {
                    if (isset($logo)) {
                        $path = Yii::$app->basePath . '/web/Logos/';
                        $filename = $logo->baseName . '.' . $logo->extension;
                        $saved = $logo->saveAs($path . $filename);
                    } else {
                        $saved = true;
                    }
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->idEmpresa]);
            } else {
                $transaction->rollback();

                $model->rutaLogo = $logoActual;
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
//        $this->findModel($id)->delete();
//        return $this->redirect(['index']);
        throw new NotFoundHttpException('Funcion no permitida.');
    }

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
