<?php

namespace app\controllers;

use Yii;
use app\models\Constants;
use app\models\Utils;
use app\models\logic\Empresa;
use app\models\logic\Usuario;
use app\models\logic\Categoria;
use app\models\logic\Documento;
use app\models\logic\Documento2;
use app\models\logic\Pago;
use app\models\search\UsuariosSearch;
use app\models\search\CategoriasSearch;
use app\models\search\DocumentosSearch;
use app\models\search\PagosSearch;
use yii\web\Controller;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * Description of DocumentosController
 *
 * @author Avuunita
 */
class DocumentosController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'menu', 'view'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-folder', 'delete-folder', 'create-template', 'delete-template'],
                        'roles' => [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create-file', 'delete-file', 'download'],
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete-folder' => ['POST'],
                    'delete-template' => ['POST'],
                    'delete-file' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex($id) {
        self::comprobarAcceso($id);

        $usuario = Usuario::findOne($id);
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        $usuariosSearch = new UsuariosSearch();
        $categoriasSearch = new CategoriasSearch();

        $params = Yii::$app->request->queryParams;
        $categoriasDataProvider = $categoriasSearch->searchMenu($params);

        if ($rol == Constants::ROL_DIRECTOR) {
            $usuariosDataProvider = $usuariosSearch->buscarUsuarios($empresa, $params);
        } else if ($rol == Constants::ROL_COORDINADOR) {
            $usuariosDataProvider = $usuariosSearch->buscarProyectosCoordinador($usuario, $params);
        } else {
            $usuariosDataProvider = null;
        }

        return $this->render('index', [
                    'usuario' => $usuario,
                    'categoriasSearch' => $categoriasSearch,
                    'categoriasDataProvider' => $categoriasDataProvider,
                    'usuariosSearch' => $usuariosSearch,
                    'usuariosDataProvider' => $usuariosDataProvider,
        ]);
    }

    public function actionMenu($id, $c) {
        self::comprobarAcceso($id);

        $usuario = Usuario::findOne($id);
        $categoriaPadre = Categoria::findOne($c);

        $categoriasSearch = new CategoriasSearch();

        $params = Yii::$app->request->queryParams;
        $categoriasDataProvider = $categoriasSearch->searchSubmenu($c, $params);

        return $this->render('menu', [
                    'usuario' => $usuario,
                    'categoriaPadre' => $categoriaPadre,
                    'categoriasSearch' => $categoriasSearch,
                    'categoriasDataProvider' => $categoriasDataProvider,
        ]);
    }

    public function actionView($id, $c, $fp = null) {
        self::comprobarAcceso($id, $fp);

        $usuario = Usuario::findOne($id);
        $rol = $usuario->authAssignments[0]->item_name;
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $documentosSearch = new DocumentosSearch();
        $params = Yii::$app->request->queryParams;

        $datosCarpetas = $documentosSearch->buscarCarpetas($usuario, $categoria, $params, $documentoPadre);
        $datosPlantillas = $documentosSearch->buscarPlantillas($usuario, $categoria, $params, $documentoPadre);
        $datosArchivos = $documentosSearch->buscarArchivos($usuario, $categoria, $params, $documentoPadre);

        return $this->render('view', [
                    'usuario' => $usuario,
                    'categoria' => $categoria,
                    'documentoPadre' => $documentoPadre,
                    'documentosSearch' => $documentosSearch,
                    'datosCarpetas' => $datosCarpetas,
                    'datosPlantillas' => $datosPlantillas,
                    'datosArchivos' => $datosArchivos,
        ]);
    }

    public function actionCreateFolder($c, $fp = null) {
        $id = Yii::$app->user->id;
        self::comprobarAccesoFicheros($id, $fp);

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
        self::comprobarPermisos($roles, $c, Constants::ACCION_CREAR_CARPETA);

        $model = new Documento();
        $model->esPlantilla = Constants::varNoBD;
        $model->esFichero = Constants::varSiBD;

        $usuario = Usuario::findOne($id);
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $this->crearCarpeta($model, $usuario, $categoria, $documentoPadre);

        return $this->render('create-folder', [
                    'model' => $model,
                    'usuario' => $usuario,
                    'categoria' => $categoria,
                    'documentoPadre' => $documentoPadre,
        ]);
    }

    public function actionCreateFile($c, $fp = null) {
        $id = Yii::$app->user->id;
        self::comprobarAccesoFicheros($id, $fp);

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR, Constants::NUM_PROYECTO];
        self::comprobarPermisos($roles, $c, Constants::ACCION_SUBIR_ARCHIVO);

        $model = new Documento2();
        $model->esPlantilla = Constants::varNoBD;
        $model->esFichero = Constants::varNoBD;

        $usuario = Usuario::findOne($id);
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $this->subirArchivo($model, $usuario, $categoria, $documentoPadre);

        return $this->render('create-file', [
                    'model' => $model,
                    'usuario' => $usuario,
                    'categoria' => $categoria,
                    'documentoPadre' => $documentoPadre,
        ]);
    }

    public function actionCreateTemplate($c, $fp = null) {
        $id = Yii::$app->user->id;
        self::comprobarAccesoFicheros($id, $fp);

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
        self::comprobarPermisos($roles, $c, Constants::ACCION_SUBIR_ARCHIVO);

        $model = new Documento2();
        $model->esPlantilla = Constants::varSiBD;
        $model->esFichero = Constants::varNoBD;

        $usuario = Usuario::findOne($id);
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $this->subirArchivo($model, $usuario, $categoria, $documentoPadre, true);

        return $this->render('create-template', [
                    'model' => $model,
                    'usuario' => $usuario,
                    'categoria' => $categoria,
                    'documentoPadre' => $documentoPadre,
        ]);
    }

    /**
     * Permite crear una carpeta en el servidor.
     * @param Documento $model
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param Documento $documentoPadre
     * @return type
     */
    private function crearCarpeta($model, $usuario, $categoria, $documentoPadre) {
        if ($model->load(Yii::$app->request->post())) {
            $folder = $model->rutaArchivo;
            $model->rutaArchivo = Utils::encrypt($model->rutaArchivo);

            $idFicheroPadre = null;
            if (isset($documentoPadre)) {
                $idFicheroPadre = $documentoPadre->idDocumento;
            }

            $model->idUsuario = $usuario->id;
            $model->idCategoria = $categoria->idCategoria;
            $model->idFicheroPadre = $idFicheroPadre;

            $model->idUsuarioRegistro = $usuario->id;
            $model->idUsuarioActualizo = $usuario->id;
            $model->accionUsuario = Constants::accionUsuarioInsertar;
            $model->fechaRegistro = date('Y-m-d H:i:s');
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $carpetas = Utils::obtenerRutaFicheros($documentoPadre);

            $path = Yii::$app->basePath . '/web/Archivos/';
            $path .= "$categoria->idCategoria/";
            for ($i = sizeof($carpetas) - 1; $i >= 0; $i--) {
                $docum = $carpetas[$i];
                $carpeta = Utils::decrypt($docum->rutaArchivo);
                $path .= "$docum->idUsuario-$carpeta/";
            }
            $path .= "$usuario->id-$folder/";

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->save(false)) {
                try {
                    $saved = FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect([
                            'view',
                            'id' => $usuario->id,
                            'c' => $categoria->idCategoria,
                            'fp' => $idFicheroPadre
                ]);
            } else {
                $transaction->rollback();

                $model->rutaArchivo = Utils::decrypt($model->rutaArchivo);
            }
        }
    }

    /**
     * Permite subir un archivo recursivamente en el servidor.
     * @param Documento $model
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param Documento $documentoPadre
     * @return type
     */
    private function subirArchivo($model, $usuario, $categoria, $documentoPadre, $esPlantilla = false) {
        if ($model->load(Yii::$app->request->post())) {
            $archivo = UploadedFile::getInstance($model, 'archivo');

            $basename = explode("____", $archivo->baseName)[0];
//            if ($esPlantilla) {
//                $basename = $basename;
//            } else {
                $time = Utils::generateTimeMark();
                $basename = $basename . "____" . $time;
//            }
            $filename = $basename . '.' . $archivo->extension;
            $model->rutaArchivo = $filename;

            $idFicheroPadre = null;
            if (isset($documentoPadre)) {
                $idFicheroPadre = $documentoPadre->idDocumento;
            }

            $model->idUsuario = $usuario->id;
            $model->idCategoria = $categoria->idCategoria;
            $model->idFicheroPadre = $idFicheroPadre;

            $model->idUsuarioRegistro = $usuario->id;
            $model->idUsuarioActualizo = $usuario->id;
            $model->accionUsuario = Constants::accionUsuarioInsertar;
            $model->fechaRegistro = date('Y-m-d H:i:s');
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $file = $model->rutaArchivo;
            $model->rutaArchivo = Utils::encrypt($model->rutaArchivo);

            $empresa = $usuario->empresa;

            $carpetas = Utils::obtenerRutaFicheros($documentoPadre);

            $path = Yii::$app->basePath . '/web/Archivos/';
            $path .= "$categoria->idCategoria/";
            for ($i = sizeof($carpetas) - 1; $i >= 0; $i--) {
                $docum = $carpetas[$i];
                $carpeta = Utils::decrypt($docum->rutaArchivo);
                $path .= "$docum->idUsuario-$carpeta/";
            }
            if (isset($empresa)) {
                $idEmpresa = $empresa->idEmpresa;
            } else {
                $idEmpresa = 0;
            }
            $path .= "$idEmpresa/";
            if ($esPlantilla) {
                $path .= Constants::directorioPlantilla;
            }

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->save(false)) {
                try {
                    $saved = FileHelper::createDirectory($path, $mode = 0775, $recursive = true);
                    $saved &= $archivo->saveAs($path . $file);
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect([
                            'view',
                            'id' => $usuario->id,
                            'c' => $categoria->idCategoria,
                            'fp' => $idFicheroPadre
                ]);
            } else {
                $transaction->rollback();

                $model->rutaArchivo = Utils::decrypt($model->rutaArchivo);
            }
        }
    }

    public function actionDownload($id, $doc) {
        self::comprobarAcceso($id);

        $documento = Documento::findOne($doc);
        if (!$this->descargarArchivo($documento)) {
            Yii::$app->session->setFlash("errordownload");
        }
    }

    /**
     * Permite descargar un archivo del servidor.
     * @param Documento $model
     * @return type
     */
    private function descargarArchivo($model) {
        $rutaArchivo = $model->rutaArchivo;
        $rutaArchivo = Utils::decrypt($rutaArchivo);

        $esPlantilla = $model->esPlantilla;
        $categoria = $model->categoria;
        $ficheroPadre = $model->ficheroPadre;
        $usuario = $model->usuario;
        $empresa = $usuario->empresa;

        $doc = $ficheroPadre;
        $carpetas = [];
        while (!is_null($doc)) {
            $carpetas[] = $doc;
            $doc = $doc->ficheroPadre;
        }

        $path = Yii::$app->basePath . '/web/Archivos/';
        $path .= "$categoria->idCategoria/";
        for ($i = sizeof($carpetas) - 1; $i >= 0; $i--) {
            $docum = $carpetas[$i];
            $carpeta = Utils::decrypt($docum->rutaArchivo);
            $path .= "$docum->idUsuario-$carpeta/";
        }
        if (isset($empresa)) {
            $idEmpresa = $empresa->idEmpresa;
        } else {
            $idEmpresa = 0;
        }
        $path .= "$idEmpresa/";
        if ($esPlantilla == Constants::varSiBD) {
            $path .= Constants::directorioPlantilla;
        }

        $file = $path . $rutaArchivo;

        try {
            // Procedemos a descargar el archivo
            // Definir headers
            $size = filesize($file);
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=\"$rutaArchivo\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: $size");
            // Descargar archivo
            readfile($file);
            return true;
        } catch (yii\base\Exception $ex) {
            throw new yii\base\Exception('No se ha podido realizar la descarga. (I)');
        } catch (yii\base\ErrorException $ex) {
            throw new yii\base\Exception('No se ha podido realizar la descarga. (II)');
        }
        return false;
    }

    public function actionDeleteFolder($doc, $c, $fp = null) {
        $id = Yii::$app->user->id;
        self::comprobarAccesoFicheros($id, $fp);

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
        self::comprobarPermisos($roles, $c, Constants::ACCION_BORRAR_CARPETA);

        $usuario = Usuario::findOne($id);
        $documento = Documento::findOne($doc);
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $this->borrarCarpeta($documento, $usuario, $categoria, $documentoPadre);
    }

    public function actionDeleteFile($doc, $c, $fp = null) {
        $id = Yii::$app->user->id;
        self::comprobarAccesoFicheros($id, $fp);

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR, Constants::NUM_PROYECTO];
        self::comprobarPermisos($roles, $c, Constants::ACCION_BORRAR_ARCHIVO);

        $usuario = Usuario::findOne($id);
        $documento = Documento::findOne($doc);
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $this->borrarArchivo($documento, $usuario, $categoria, $documentoPadre);
    }

    public function actionDeleteTemplate($doc, $c, $fp = null) {
        $id = Yii::$app->user->id;
        self::comprobarAccesoFicheros($id, $fp);

        $roles = [Constants::NUM_SUPERVISOR, Constants::NUM_DIRECTOR, Constants::NUM_COORDINADOR];
        self::comprobarPermisos($roles, $c, Constants::ACCION_BORRAR_PLANTILLA);

        $usuario = Usuario::findOne($id);
        $documento = Documento::findOne($doc);
        $categoria = Categoria::findOne($c);

        $documentoPadre = null;
        if (isset($fp)) {
            $documentoPadre = Documento::findOne($fp);
        }

        $this->borrarArchivo($documento, $usuario, $categoria, $documentoPadre);
    }

    /**
     * Permite borrar una carpeta (de la BD, mas no del servidor), SI Y SOLO SI no hay contenido en esa carpeta.
     * @param Documento $model
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param Documento $documentoPadre
     */
    private function borrarCarpeta($model, $usuario, $categoria, $documentoPadre) {
        $documentosSearch = new DocumentosSearch();
        $params = Yii::$app->request->queryParams;

        $hasSomething = $documentosSearch->validarContenidoCarpeta($usuario, $model, $params);
        if ($hasSomething) {
            throw new ForbiddenHttpException('Esta carpeta tiene contenido. No se puede borrar.');
        }

        $idFicheroPadre = null;
        if (isset($documentoPadre)) {
            $idFicheroPadre = $documentoPadre->idDocumento;
        }

        $model->eliminado = Constants::varSiBD;

        $model->idUsuarioActualizo = Yii::$app->user->id;
        $model->accionUsuario = Constants::accionUsuarioBorrar;
        $model->fechaModificacion = date('Y-m-d H:i:s');

        if ($model->save()) {
            return $this->redirect([
                        'view',
                        'id' => $usuario->id,
                        'c' => $categoria->idCategoria,
                        'fp' => $idFicheroPadre
            ]);
        }
    }

    /**
     * Permite borrar un archivo (de la BD, mas no del servidor).
     * @param Documento $model
     * @param Usuario $usuario
     * @param Categoria $categoria
     * @param Documento $documentoPadre
     */
    private function borrarArchivo($model, $usuario, $categoria, $documentoPadre) {
        $idFicheroPadre = null;
        if (isset($documentoPadre)) {
            $idFicheroPadre = $documentoPadre->idDocumento;
        }

        $model->eliminado = Constants::varSiBD;

        $model->idUsuarioActualizo = Yii::$app->user->id;
        $model->accionUsuario = Constants::accionUsuarioBorrar;
        $model->fechaModificacion = date('Y-m-d H:i:s');

        if ($model->save()) {
            return $this->redirect([
                        'view',
                        'id' => $usuario->id,
                        'c' => $categoria->idCategoria,
                        'fp' => $idFicheroPadre
            ]);
        }
    }

    /**
     * 
     * @param string[] $roles
     * @param type $c
     * @param int $accion
     */
    private static function comprobarPermisos($roles, $c, $accion) {
        $categoria = Categoria::findOne($c);

        $resp = Utils::validarPermisosCategoria($roles, $categoria, $accion);

        if ($resp != 1) {
            if ($resp === "ROL") {
                throw new NotFoundHttpException('No ha ingresado roles válidos.');
            }
            if ($resp === "ACCION") {
                throw new NotFoundHttpException('No ha ingresado una acción.');
            }
            if ($resp === "PERMISO") {
                throw new NotFoundHttpException("La categoría $categoria->nombre no tiene definido permisos.");
            }
            if ($resp === "ACCION2") {
                throw new NotFoundHttpException('No ha ingresado una acción válida.');
            }
            if ($resp === 0) {
                throw new ForbiddenHttpException('No tiene acceso a esa acción.');
            }
        }
    }

    private static function comprobarAcceso($idUsuario, $idFicheroPadre = null) {
        if (is_null($idUsuario) || (!is_null($idUsuario) && strlen(trim($idUsuario)) == 0)) {
            throw new NotFoundHttpException('Usuario no ingresado.');
        }

        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        self::comprobarAccesoPagos($empresa);

        $usuarioIngresado = Usuario::findOne($idUsuario);
        if (!isset($usuarioIngresado)) {
            throw new NotFoundHttpException('Usuario no encontrado.');
        }
        $rolIngresado = $usuarioIngresado->authAssignments[0]->item_name;
        $empresaIngresada = $usuarioIngresado->empresa;

        switch ($rol) {
            case Constants::ROL_SUPERVISOR:
                if ($rolIngresado != Constants::ROL_SUPERVISOR) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (I)');
                }
                break;
            case Constants::ROL_DIRECTOR:
                if ($rolIngresado != Constants::ROL_SUPERVISOR && $idUsuario != $id && $empresaIngresada->idEmpresa != $empresa->idEmpresa) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (II)');
                } else if ($rolIngresado == Constants::ROL_SUPERVISOR) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (III)');
                }
                break;
            case Constants::ROL_COORDINADOR:
                if ($rolIngresado != Constants::ROL_SUPERVISOR && $idUsuario != $id && $empresaIngresada->idEmpresa != $empresa->idEmpresa) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (IV)');
                } else if ($rolIngresado == Constants::ROL_SUPERVISOR || $rolIngresado == Constants::ROL_DIRECTOR) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (V)');
                } else if ($rolIngresado == Constants::ROL_COORDINADOR && $id != $idUsuario) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (VI)');
                } else if ($rolIngresado == Constants::ROL_PROYECTO && $id != $usuarioIngresado->idUsuarioRegistro) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (VII)');
                }
                break;
            case Constants::ROL_PROYECTO:
                if ($rolIngresado != Constants::ROL_SUPERVISOR && $idUsuario != $id && $empresaIngresada->idEmpresa != $empresa->idEmpresa) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (VIII)');
                } else if ($rolIngresado != Constants::ROL_PROYECTO) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (IX)');
                } else if ($rolIngresado == Constants::ROL_PROYECTO && $id != $idUsuario) {
                    throw new ForbiddenHttpException('No tiene acceso a esa información. (X)');
                }
                break;
        }

        self::comprobarAccesoFicheros($id, $idFicheroPadre);
    }

    private static function comprobarAccesoFicheros($id, $idFicheroPadre) {
        $usuario = Usuario::findOne($id);
        $rol = $usuario->authAssignments[0]->item_name;
        $empresa = $usuario->empresa;

        self::comprobarAccesoPagos($empresa);

        if (isset($idFicheroPadre)) {
            $documentoPadre = Documento::findOne($idFicheroPadre);
            if (!isset($documentoPadre)) {
                throw new NotFoundHttpException('Directorio no encontrado.');
            }
            if ($documentoPadre->esFichero == Constants::varNoBD) {
                throw new ForbiddenHttpException('No se pueden ver, crear o borrar archivos dentro de otro archivo.');
            }
            $usuarioDoc = $documentoPadre->usuario;
            $idUsuarioDoc = $usuarioDoc->id;
            $rolDoc = $usuarioDoc->authAssignments[0]->item_name;
            $empresaDoc = $usuarioDoc->empresa;
            switch ($rol) {
                case Constants::ROL_SUPERVISOR:
                    if ($rolDoc != Constants::ROL_SUPERVISOR) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XI)');
                    }
                    break;
                case Constants::ROL_DIRECTOR:
                    if ($rolDoc != Constants::ROL_SUPERVISOR && $idUsuarioDoc != $id && $empresa->idEmpresa != $empresaDoc->idEmpresa) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XII)');
                    }
                    break;
                case Constants::ROL_COORDINADOR:
                    if ($rolDoc != Constants::ROL_SUPERVISOR && $idUsuarioDoc != $id && $empresa->idEmpresa != $empresaDoc->idEmpresa) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XIII)');
                    } else if ($rolDoc == Constants::ROL_COORDINADOR && $id != $idUsuarioDoc) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XIV)');
                    } else if ($rolDoc == Constants::ROL_PROYECTO && $id != $usuarioDoc->idUsuarioRegistro) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XV)');
                    }
                    break;
                case Constants::ROL_PROYECTO:
                    if ($rolDoc != Constants::ROL_SUPERVISOR && $idUsuarioDoc != $id && $empresa->idEmpresa != $empresaDoc->idEmpresa) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XVI)');
                    } else if ($rolDoc == Constants::ROL_PROYECTO && $id != $idUsuarioDoc) {
                        throw new ForbiddenHttpException('No tiene acceso a esa información. (XVII)');
                    }
                    break;
            }
        }
    }

    /**
     * 
     * @param Empresa $empresa
     * @throws ForbiddenHttpException
     */
    private static function comprobarAccesoPagos($empresa) {
        $params = Yii::$app->request->queryParams;

        if ($empresa->idEmpresa != 1) {
            $pagosSearch = new PagosSearch();
            $pagos = $pagosSearch->buscarEstadoCuenta($empresa, $params)->getModels();

            if (!(isset($pagos) && sizeof($pagos) > 0)) {
                throw new ForbiddenHttpException('No se han encontrado pagos recientes, por tanto no puede ver el contenido de esta página.');
            }

            $found = false;
            foreach ($pagos as $pago) {
                /* @var $pago Pago */
                $fechaInicial = new \DateTime($pago->fechaInicial);
                $fechaFinal = new \DateTime($pago->fechaFinal);
                $hoy = new \DateTime();
                if ($fechaInicial <= $hoy && $hoy <= $fechaFinal) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                throw new ForbiddenHttpException('No ha efectuado su pago correspondiente. No puede ver el contenido de esta página.');
            }
        }
    }

}
