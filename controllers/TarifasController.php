<?php

namespace app\controllers;

use Yii;
use app\models\Constants;
use app\models\logic\Tarifa;
use app\models\search\TarifasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TarifasController implements the CRUD actions for Tarifa model.
 */
class TarifasController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Muestra todas las tarifas activas registradas.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new TarifasSearch();

        $param = Yii::$app->request->queryParams;
        $tarifasActivas = $searchModel->buscarTarifas(Constants::estadoActivoBD, $param);
        $tarifasInactivas = $searchModel->buscarTarifas(Constants::estadoInactivoBD, $param);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'tarifasActivas' => $tarifasActivas,
                    'tarifasInactivas' => $tarifasInactivas,
        ]);
    }

    /**
     * Muestra la información específica de una tarifa.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Registra una tarifa de pagos.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Tarifa();
        $model->estado = Constants::estadoActivoBD;
        $model->diasAplica = 0;
        $model->mesesAplica = 0;
        $logoActual = $model->rutaImagen;

        if ($model->load(Yii::$app->request->post())) {
            $logo = UploadedFile::getInstance($model, 'rutaImagen');
            if (isset($logo)) {
                $model->rutaImagen = $logo->name;
            } else {
                $model->rutaImagen = $logoActual;
            }

            $idUsuario = Yii::$app->user->id;

            $model->idUsuarioRegistro = $idUsuario;
            $model->idUsuarioActualizo = $idUsuario;
            $model->accionUsuario = Constants::accionUsuarioInsertar;
            $model->fechaCreacion = date('Y-m-d H:i:s');
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->save()) {
                try {
                    if (isset($logo)) {
                        $path = Yii::$app->basePath . '/web/Tarifas/';
                        $filename = $logo->baseName . '.' . $logo->extension;
                        $saved = $logo->saveAs($path . $filename);
                    } else {
                        $saved = true;
                    }
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->idTarifa]);
            } else {
                $transaction->rollback();

                $model->rutaLogo = $logoActual;
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Modifica una tarifa de pagos.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->estado == Constants::estadoInactivoBD) {
            throw new NotFoundHttpException("Esta tarifa ya está inactiva.");
        }

        $logoActual = $model->rutaImagen;

        if ($model->load(Yii::$app->request->post())) {
            $logo = UploadedFile::getInstance($model, 'rutaImagen');
            if (isset($logo)) {
                $model->rutaImagen = $logo->name;
            } else {
                $model->rutaImagen = $logoActual;
            }

            $idUsuario = Yii::$app->user->id;

            $model->idUsuarioActualizo = $idUsuario;
            $model->accionUsuario = Constants::accionUsuarioModificar;
            $model->fechaModificacion = date('Y-m-d H:i:s');

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $saved = false;

            if ($model->update()) {
                try {
                    if (isset($logo)) {
                        $path = Yii::$app->basePath . '/web/Tarifas/';
                        $filename = $logo->baseName . '.' . $logo->extension;
                        $saved = $logo->saveAs($path . $filename);
                    } else {
                        $saved = true;
                    }
                } catch (yii\base\Exception $ex) {
                    $saved = false;
                }
            }

            if ($saved) {
                $transaction->commit();

                return $this->redirect(['view', 'id' => $model->idTarifa]);
            } else {
                $transaction->rollback();

                $model->rutaLogo = $logoActual;
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Borra (inactiva) una tarifa seleccionada.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->estado == Constants::estadoInactivoBD) {
            throw new NotFoundHttpException("Esta tarifa ya está inactiva.");
        }

        $model->estado = Constants::estadoInactivoBD;

        $model->idUsuarioActualizo = Yii::$app->user->id;
        $model->accionUsuario = Constants::accionUsuarioBorrar;
        $model->fechaModificacion = date('Y-m-d H:i:s');

        if ($model->save()) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Tarifa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tarifa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Tarifa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
