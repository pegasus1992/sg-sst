<?php

namespace app\controllers;

use Yii;
use app\models\Constants;
use app\models\logic\Empresa;
use app\models\logic\Pago;
use app\models\logic\Tarifa;
use app\models\logic\Usuario;
use app\models\search\PagosSearch;
use app\models\search\TarifasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * PagosController implements the CRUD actions for Pago model.
 */
class PagosController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'delete'],
                        'roles' => [Constants::NUM_SUPERVISOR],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['historial', 'ofertas', 'oferta', 'pagar'],
                        'roles' => [Constants::NUM_DIRECTOR],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'pagar' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Muestra todos los pagos realizados (activos y anulados).
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PagosSearch();

        $param = Yii::$app->request->queryParams;
        $pagosActivos = $searchModel->buscar(Constants::estadoActivoBD, $param);
        $pagosAnulados = $searchModel->buscar(Constants::estadoAnuladoBD, $param);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'pagosActivos' => $pagosActivos,
                    'pagosAnulados' => $pagosAnulados,
        ]);
    }

    /**
     * Muestra la información específica de un pago.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Registra el pago de una empresa.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Pago();
        $empresas = $this->getEmpresas();
        $tarifas = $this->getTarifas();

        $model->porcentajeIva = Constants::PORCENTAJE_IVA_DEFAULT;
        $model->valorDescuento = Constants::VALOR_DESCUENTO_DEFAULT;

        if ($model->load(Yii::$app->request->post())) {
            if ($this->pagar($model)) {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->idPago]);
                }
            } else {
                throw new \yii\web\ServerErrorHttpException("No se ha recibido el pago.");
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'empresas' => $empresas,
                    'tarifas' => $tarifas,
        ]);
    }

    /**
     * 
     * @param Pago $model
     */
    private function pagar($model) {
        $params = Yii::$app->request->queryParams;

        $idUsuario = Yii::$app->user->id;
        $tarifa = $model->tarifa;

        $fechas = (new PagosSearch())->traerFechasPago($model->empresa, $tarifa, $params);

        $model->fechaInicial = $fechas['fechaInicial']->format('Y-m-d H:i:s');
        $model->fechaFinal = $fechas['fechaFinal']->format('Y-m-d H:i:s');

        $model->idUsuarioRegistro = $idUsuario;
        $model->idUsuarioActualizo = $idUsuario;
        $model->accionUsuario = Constants::accionUsuarioInsertar;
        $model->fechaCreacion = date('Y-m-d H:i:s');
        $model->fechaModificacion = date('Y-m-d H:i:s');

        return true;
    }

    /**
     * Borra (anula) un pago realizado previamente.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);

        if ($model->estado == Constants::estadoAnuladoBD) {
            throw new NotFoundHttpException("Este pago ya está anulado.");
        }

        $model->estado = Constants::estadoAnuladoBD;

        $model->idUsuarioActualizo = Yii::$app->user->id;
        $model->accionUsuario = Constants::accionUsuarioAnular;
        $model->fechaModificacion = date('Y-m-d H:i:s');

        if ($model->save()) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Muestra todos los pagos realizados (activos y anulados).
     * @return mixed
     */
    public function actionHistorial() {
        $searchModel = new PagosSearch();

        $param = Yii::$app->request->queryParams;

        $id = Yii::$app->user->id;
        $usuario = Usuario::findOne($id);
        $empresa = $usuario->empresa;
        $pagos = $searchModel->buscarHistorialPagos($empresa, $param);

        return $this->render('historial', [
                    'searchModel' => $searchModel,
                    'empresa' => $empresa,
                    'pagos' => $pagos,
        ]);
    }

    /**
     * Muestra todas las ofertas registradas.
     * @return mixed
     */
    public function actionOfertas() {
        $searchModel = new TarifasSearch();

        $param = Yii::$app->request->queryParams;
        $tarifas = $searchModel->buscarOfertas($param);

        return $this->render('ofertas', [
                    'searchModel' => $searchModel,
                    'tarifas' => $tarifas,
        ]);
    }

    /**
     * Muestra la información específica de la oferta seleccionada.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionOferta($id) {
        return $this->render('oferta', [
                    'model' => $this->findModel2($id),
        ]);
    }

    /**
     * 
     * @param type $id
     */
    public function actionPagar($id) {
        $idUsuario = Yii::$app->user->id;
        $usuario = Usuario::findOne($idUsuario);
        $empresa = $usuario->empresa;

        $model = new Pago();

        $model->porcentajeIva = Constants::PORCENTAJE_IVA_DEFAULT;
        $model->valorDescuento = Constants::VALOR_DESCUENTO_DEFAULT;
        $model->idTarifa = $id;
        $model->idEmpresa = $empresa->idEmpresa;

        if ($this->pagar($model)) {
            // A lo que siga :'v
        } else {
            throw new \yii\web\ServerErrorHttpException("No se ha recibido el pago.");
        }
    }

    /**
     * Finds the Pago model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pago the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Pago::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Tarifa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tarifa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel2($id) {
        if (($model = Tarifa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function getEmpresas() {
        return ArrayHelper::map(Empresa::find()
                                ->where("idEmpresa <> 1")// Excluir empresa supervisora (nosotros)
                                ->all(), 'idEmpresa', 'nombre');
    }

    private function getTarifas() {
        $search = new TarifasSearch();
        $tarifas = $search->buscarTarifas(Constants::estadoActivoBD, Yii::$app->request->queryParams)->getModels();
        $resp = [];
        foreach ($tarifas as $tarifa) {
            $id = $tarifa->idTarifa;
            $codigo = $tarifa->codigo;
            $nombre = $tarifa->nombre;
            $valor = number_format($tarifa->valor);
            $resp[$id] = "$codigo - $nombre - $ $valor";
        }
        return $resp;
    }

}
