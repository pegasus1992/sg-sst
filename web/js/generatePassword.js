function generatePassword() {
    var password = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    return password;
}

document.getElementById("generate_password").onclick = function () {
    document.getElementById("password").value = generatePassword();
};